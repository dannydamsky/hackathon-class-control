package com.hackathon.project.classcontrol.ui.activity.chat.adapter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.gson.Gson;
import com.hackathon.project.classcontrol.GlideApp;
import com.hackathon.project.classcontrol.R;
import com.hackathon.project.classcontrol.data.db.remote.FirebaseAuthHelper;
import com.hackathon.project.classcontrol.data.db.remote.FirebaseDbHelper;
import com.hackathon.project.classcontrol.data.db.remote.MessageType;
import com.hackathon.project.classcontrol.data.db.remote.model.Chat;
import com.hackathon.project.classcontrol.data.db.remote.model.User;
import com.hackathon.project.classcontrol.ui.widget.CanvasView;
import com.hackathon.project.classcontrol.util.ImageUtils;
import com.hackathon.project.classcontrol.util.TimestampUtils;
import com.hackathon.project.classcontrol.util.metadata.canvas.CanvasMessage;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public final class ChatAdapter extends FirestoreRecyclerAdapter<Chat, RecyclerView.ViewHolder> {

    private static final int VIEW_TYPE_RECIPIENT_IMAGE_NEW = 0;
    private static final int VIEW_TYPE_RECIPIENT_IMAGE_OLD = 1;
    private static final int VIEW_TYPE_RECIPIENT_MESSAGE_NEW = 2;
    private static final int VIEW_TYPE_RECIPIENT_MESSAGE_OLD = 3;
    private static final int VIEW_TYPE_SENDER_IMAGE = 4;
    private static final int VIEW_TYPE_SENDER_MESSAGE = 5;
    private static final int VIEW_TYPE_RECIPIENT_CANVAS_NEW = 6;
    private static final int VIEW_TYPE_RECIPIENT_CANVAS_OLD = 7;
    private static final int VIEW_TYPE_SENDER_CANVAS = 8;

    @NonNull
    public static ChatAdapter newInstance(@NonNull OnChatClickListener listener, @NonNull String classroomId) {
        Query query = FirebaseDbHelper.getClassroomChatsQuery(classroomId);
        FirestoreRecyclerOptions<Chat> options = new FirestoreRecyclerOptions.Builder<Chat>()
                .setQuery(query, Chat.class)
                .build();
        return new ChatAdapter(options, listener);
    }

    private OnChatClickListener listener;
    private Context context;

    private ChatAdapter(@NonNull FirestoreRecyclerOptions<Chat> options, @NonNull OnChatClickListener listener) {
        super(options);
        setHasStableIds(true);
        this.listener = listener;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        Chat lastModel;
        if (position == 0)
            lastModel = null;
        else
            lastModel = getItem(position - 1);

        Chat newModel = getItem(position);
        int viewType;
        if (newModel.getSenderId().equals(FirebaseAuthHelper.getUserId())) {
            viewType = returnViewTypeForSender(newModel);
        } else if (lastModel == null || !lastModel.getSenderId().equals(newModel.getSenderId())) {
            viewType = returnViewTypeForNewRecipient(newModel);
        } else {
            viewType = returnViewTypeForOldRecipient(newModel);
        }
        return viewType;
    }

    private int returnViewTypeForSender(Chat newModel) {
        MessageType messageType = MessageType.valueOf(newModel.getMessageType());
        int viewType;
        switch (messageType) {
            case TEXT:
                viewType = VIEW_TYPE_SENDER_MESSAGE;
                break;
            case IMAGE:
                viewType = VIEW_TYPE_SENDER_IMAGE;
                break;
            case CANVAS:
                viewType = VIEW_TYPE_SENDER_CANVAS;
                break;
            default:
                viewType = VIEW_TYPE_SENDER_MESSAGE;
                break;
        }
        return viewType;
    }

    private int returnViewTypeForNewRecipient(Chat newModel) {
        MessageType messageType = MessageType.valueOf(newModel.getMessageType());
        int viewType;
        switch (messageType) {
            case TEXT:
                viewType = VIEW_TYPE_RECIPIENT_MESSAGE_NEW;
                break;
            case IMAGE:
                viewType = VIEW_TYPE_RECIPIENT_IMAGE_NEW;
                break;
            case CANVAS:
                viewType = VIEW_TYPE_RECIPIENT_CANVAS_NEW;
                break;
            default:
                viewType = VIEW_TYPE_RECIPIENT_MESSAGE_NEW;
                break;
        }
        return viewType;
    }

    private int returnViewTypeForOldRecipient(Chat newModel) {
        MessageType messageType = MessageType.valueOf(newModel.getMessageType());
        int viewType;
        switch (messageType) {
            case TEXT:
                viewType = VIEW_TYPE_RECIPIENT_MESSAGE_OLD;
                break;
            case IMAGE:
                viewType = VIEW_TYPE_RECIPIENT_IMAGE_OLD;
                break;
            case CANVAS:
                viewType = VIEW_TYPE_RECIPIENT_CANVAS_OLD;
                break;
            default:
                viewType = VIEW_TYPE_RECIPIENT_MESSAGE_OLD;
                break;
        }
        return viewType;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        this.context = parent.getContext();
        RecyclerView.ViewHolder holder;
        switch (viewType) {
            case VIEW_TYPE_RECIPIENT_IMAGE_NEW:
                holder = getViewHolderNewRecipientImage(parent);
                break;
            case VIEW_TYPE_RECIPIENT_IMAGE_OLD:
                holder = getViewHolderOldRecipientImage(parent);
                break;
            case VIEW_TYPE_RECIPIENT_MESSAGE_NEW:
                holder = getViewHolderNewRecipientMessage(parent);
                break;
            case VIEW_TYPE_RECIPIENT_MESSAGE_OLD:
                holder = getViewHolderOldRecipientMessage(parent);
                break;
            case VIEW_TYPE_SENDER_IMAGE:
                holder = getViewHolderSenderImage(parent);
                break;
            case VIEW_TYPE_SENDER_MESSAGE:
                holder = getViewHolderSenderMessage(parent);
                break;
            case VIEW_TYPE_RECIPIENT_CANVAS_NEW:
                holder = getViewHolderNewRecipientCanvas(parent);
                break;
            case VIEW_TYPE_RECIPIENT_CANVAS_OLD:
                holder = getViewHolderOldRecipientCanvas(parent);
                break;
            case VIEW_TYPE_SENDER_CANVAS:
                holder = getViewHolderSenderCanvas(parent);
                break;
            default:
                holder = getViewHolderNewRecipientMessage(parent);
        }
        return holder;
    }

    private ViewHolderNewRecipientImage getViewHolderNewRecipientImage(@NonNull ViewGroup parent) {
        View itemView = getInflatedView(parent, R.layout.row_image_new_recipient);
        return new ViewHolderNewRecipientImage(itemView);
    }

    private ViewHolderOldRecipientImage getViewHolderOldRecipientImage(@NonNull ViewGroup parent) {
        View itemView = getInflatedView(parent, R.layout.row_image_old_recipient);
        return new ViewHolderOldRecipientImage(itemView);
    }

    private ViewHolderNewRecipientMessage getViewHolderNewRecipientMessage(@NonNull ViewGroup parent) {
        View itemView = getInflatedView(parent, R.layout.row_message_new_recipient);
        return new ViewHolderNewRecipientMessage(itemView);
    }

    private ViewHolderOldRecipientMessage getViewHolderOldRecipientMessage(@NonNull ViewGroup parent) {
        View itemView = getInflatedView(parent, R.layout.row_message_old_recipient);
        return new ViewHolderOldRecipientMessage(itemView);
    }

    private ViewHolderSenderImage getViewHolderSenderImage(@NonNull ViewGroup parent) {
        View itemView = getInflatedView(parent, R.layout.row_image_self);
        return new ViewHolderSenderImage(itemView);
    }

    private ViewHolderSenderMessage getViewHolderSenderMessage(@NonNull ViewGroup parent) {
        View itemView = getInflatedView(parent, R.layout.row_message_self);
        return new ViewHolderSenderMessage(itemView);
    }

    private ViewHolderSenderCanvas getViewHolderSenderCanvas(@NonNull ViewGroup parent) {
        View itemView = getInflatedView(parent, R.layout.row_canvas_self);
        return new ViewHolderSenderCanvas(itemView);
    }

    private ViewHolderNewRecipientCanvas getViewHolderNewRecipientCanvas(@NonNull ViewGroup parent) {
        View itemView = getInflatedView(parent, R.layout.row_canvas_new_recipient);
        return new ViewHolderNewRecipientCanvas(itemView);
    }

    private ViewHolderOldRecipientCanvas getViewHolderOldRecipientCanvas(@NonNull ViewGroup parent) {
        View itemView = getInflatedView(parent, R.layout.row_canvas_old_recipient);
        return new ViewHolderOldRecipientCanvas(itemView);
    }

    private View getInflatedView(@NonNull ViewGroup parent, @LayoutRes int layoutId) {
        return LayoutInflater.from(context).inflate(layoutId, parent, false);
    }

    @Override
    protected void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position, @NonNull Chat model) {
        switch (holder.getItemViewType()) {
            case VIEW_TYPE_RECIPIENT_IMAGE_NEW:
                bindRecipientImageNew((ViewHolderNewRecipientImage) holder, model);
                break;
            case VIEW_TYPE_RECIPIENT_IMAGE_OLD:
                bindRecipientImageOld((ViewHolderOldRecipientImage) holder, model);
                break;
            case VIEW_TYPE_RECIPIENT_MESSAGE_NEW:
                bindRecipientMessageNew((ViewHolderNewRecipientMessage) holder, model);
                break;
            case VIEW_TYPE_RECIPIENT_MESSAGE_OLD:
                bindRecipientMessageOld((ViewHolderOldRecipientMessage) holder, model);
                break;
            case VIEW_TYPE_SENDER_IMAGE:
                bindSenderImage((ViewHolderSenderImage) holder, model);
                break;
            case VIEW_TYPE_SENDER_MESSAGE:
                bindSenderMessage((ViewHolderSenderMessage) holder, model);
                break;
            case VIEW_TYPE_RECIPIENT_CANVAS_NEW:
                bindRecipientCanvasNew((ViewHolderNewRecipientCanvas) holder, model);
                break;
            case VIEW_TYPE_RECIPIENT_CANVAS_OLD:
                bindRecipientCanvasOld((ViewHolderOldRecipientCanvas) holder, model);
                break;
            case VIEW_TYPE_SENDER_CANVAS:
                bindSenderCanvas((ViewHolderSenderCanvas) holder, model);
                break;
            default:
                bindRecipientMessageNew((ViewHolderNewRecipientMessage) holder, model);
        }
    }

    private void bindRecipientCanvasNew(@NonNull ViewHolderNewRecipientCanvas holder, @NonNull Chat model) {
        setMessageSentTime(holder.senderImageSentTime, model);
        loadExtraInfoFromUserQuery(holder.senderIcon, holder.senderName, model);
        configureCanvas(holder.layoutContainer, holder.senderImage, model);
    }

    private void bindRecipientCanvasOld(@NonNull ViewHolderOldRecipientCanvas holder, @NonNull Chat model) {
        setMessageSentTime(holder.senderImageSentTime, model);
        configureCanvas(holder.layoutContainer, holder.senderImage, model);
    }

    private void bindSenderCanvas(@NonNull ViewHolderSenderCanvas holder, @NonNull Chat model) {
        setMessageSentTime(holder.sentImageSentTime, model);
        configureCanvas(holder.layoutContainer, holder.sentImage, model);
    }

    private void configureCanvas(@NonNull ConstraintLayout layoutContainer, @NonNull CanvasView canvasView, @NonNull Chat model) {
        CanvasMessage message = new Gson().fromJson(model.getMessage(), CanvasMessage.class);
        canvasView.post(() -> {
            canvasView.setCanvasMessage(message);
            canvasView.setPaintColorResource(R.color.transparent);
            layoutContainer.setOnClickListener(v -> listener.onEditCanvasClicked(model));
            canvasView.setOnClickListener(v -> listener.onEditCanvasClicked(model));
        });
    }

    private void bindRecipientImageNew(@NonNull ViewHolderNewRecipientImage holder, @NonNull Chat model) {
        loadImageFromMessage(holder.senderImage, model);
        setMessageSentTime(holder.senderImageSentTime, model);
        loadExtraInfoFromUserQuery(holder.senderIcon, holder.senderName, model);
    }

    private void bindRecipientImageOld(@NonNull ViewHolderOldRecipientImage holder, @NonNull Chat model) {
        loadImageFromMessage(holder.senderImage, model);
        setMessageSentTime(holder.senderImageSentTime, model);
    }

    private void bindRecipientMessageNew(@NonNull ViewHolderNewRecipientMessage holder, @NonNull Chat model) {
        holder.senderMessage.setText(model.getMessage());
        setMessageSentTime(holder.senderMessageSentTime, model);
        loadExtraInfoFromUserQuery(holder.senderIcon, holder.senderName, model);
    }

    private void bindRecipientMessageOld(@NonNull ViewHolderOldRecipientMessage holder, @NonNull Chat model) {
        holder.senderMessage.setText(model.getMessage());
        setMessageSentTime(holder.senderMessageSentTime, model);
    }

    private void bindSenderImage(@NonNull ViewHolderSenderImage holder, @NonNull Chat model) {
        loadImageFromMessage(holder.sentImage, model);
        setMessageSentTime(holder.sentImageSentTime, model);
    }

    private void bindSenderMessage(@NonNull ViewHolderSenderMessage holder, @NonNull Chat model) {
        holder.sentMessage.setText(model.getMessage());
        setMessageSentTime(holder.sentMessageSentTime, model);
    }

    private void loadExtraInfoFromUserQuery(ImageView imageView, TextView textView, @NonNull Chat model) {
        imageView.setImageResource(R.drawable.placeholder_user_icon);
        textView.setText(R.string.loading);
        FirebaseDbHelper.getUserFromClassroomByUserIdQuery(model.getSenderId(), model.getClassroomId()).get()
                .addOnSuccessListener(queryDocumentSnapshots ->
                        bindInfoFromUserQuery(imageView, textView, queryDocumentSnapshots))
                .addOnFailureListener(e -> Log.e("ERROR", e.getMessage()));
    }

    private void bindInfoFromUserQuery(ImageView imageView, TextView textView, QuerySnapshot queryDocumentSnapshots) {
        List<DocumentSnapshot> snapshots = queryDocumentSnapshots.getDocuments();
        User user = snapshots.get(0).toObject(User.class);
        if (user != null) {
            loadInfoFromUser(imageView, textView, user);
        }
    }

    private void loadInfoFromUser(ImageView imageView, TextView textView, User user) {
        try {
            initiateLoadingInfo(imageView, textView, user);
        } catch (IllegalArgumentException e) {
            Log.e("ERROR", e.getMessage());
        }
    }

    private void initiateLoadingInfo(ImageView imageView, TextView textView, User user) throws IllegalArgumentException {
        GlideApp.with(context).load(user.getPhotoUrl())
                .circleCrop()
                .into(imageView);
        textView.setText(user.getDisplayName());
    }

    private void loadImageFromMessage(ImageView imageView, Chat model) {
        GlideApp.with(context)
                .load(ImageUtils.stringToBitmap(model.getMessage()))
                .placeholder(R.drawable.image_tmp)
                .into(imageView);
        setImageViewOnClickListener(imageView, model);
    }

    private void setImageViewOnClickListener(ImageView imageView, Chat model) {
        imageView.setOnClickListener(v -> listener.onViewImageClicked(model));
    }

    private void setMessageSentTime(TextView textView, Chat model) {
        long time = model.getSentTime().getTime();
        String timeText = TimestampUtils.convertTimestampToTime(time);
        textView.setText(timeText);
    }

    static final class ViewHolderNewRecipientImage extends RecyclerView.ViewHolder {
        @BindView(R.id.senderIcon)
        ImageView senderIcon;
        @BindView(R.id.senderName)
        TextView senderName;
        @BindView(R.id.senderImage)
        ImageView senderImage;
        @BindView(R.id.senderImageSentTime)
        TextView senderImageSentTime;

        private ViewHolderNewRecipientImage(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    static final class ViewHolderNewRecipientCanvas extends RecyclerView.ViewHolder {
        @BindView(R.id.senderIcon)
        ImageView senderIcon;
        @BindView(R.id.senderName)
        TextView senderName;
        @BindView(R.id.layoutContainer)
        ConstraintLayout layoutContainer;
        @BindView(R.id.senderImage)
        CanvasView senderImage;
        @BindView(R.id.senderImageSentTime)
        TextView senderImageSentTime;

        private ViewHolderNewRecipientCanvas(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    static final class ViewHolderOldRecipientImage extends RecyclerView.ViewHolder {
        @BindView(R.id.senderImage)
        ImageView senderImage;
        @BindView(R.id.senderImageSentTime)
        TextView senderImageSentTime;

        private ViewHolderOldRecipientImage(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    static final class ViewHolderOldRecipientCanvas extends RecyclerView.ViewHolder {
        @BindView(R.id.layoutContainer)
        ConstraintLayout layoutContainer;
        @BindView(R.id.senderImage)
        CanvasView senderImage;
        @BindView(R.id.senderImageSentTime)
        TextView senderImageSentTime;

        private ViewHolderOldRecipientCanvas(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    static final class ViewHolderNewRecipientMessage extends RecyclerView.ViewHolder {
        @BindView(R.id.senderIcon)
        ImageView senderIcon;
        @BindView(R.id.senderName)
        TextView senderName;
        @BindView(R.id.senderMessage)
        TextView senderMessage;
        @BindView(R.id.senderMessageSentTime)
        TextView senderMessageSentTime;

        private ViewHolderNewRecipientMessage(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    static final class ViewHolderOldRecipientMessage extends RecyclerView.ViewHolder {
        @BindView(R.id.senderMessage)
        TextView senderMessage;
        @BindView(R.id.senderMessageSentTime)
        TextView senderMessageSentTime;

        private ViewHolderOldRecipientMessage(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    static final class ViewHolderSenderImage extends RecyclerView.ViewHolder {
        @BindView(R.id.sentImage)
        ImageView sentImage;
        @BindView(R.id.sentImageSentTime)
        TextView sentImageSentTime;

        private ViewHolderSenderImage(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    static final class ViewHolderSenderCanvas extends RecyclerView.ViewHolder {
        @BindView(R.id.layoutContainer)
        ConstraintLayout layoutContainer;
        @BindView(R.id.sentImage)
        CanvasView sentImage;
        @BindView(R.id.sentImageSentTime)
        TextView sentImageSentTime;

        private ViewHolderSenderCanvas(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    static final class ViewHolderSenderMessage extends RecyclerView.ViewHolder {
        @BindView(R.id.sentMessage)
        TextView sentMessage;
        @BindView(R.id.sentMessageSentTime)
        TextView sentMessageSentTime;

        private ViewHolderSenderMessage(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface OnChatClickListener {
        void onViewImageClicked(Chat model);

        void onEditCanvasClicked(Chat model);
    }
}
