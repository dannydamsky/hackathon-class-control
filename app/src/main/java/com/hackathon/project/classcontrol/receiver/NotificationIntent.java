package com.hackathon.project.classcontrol.receiver;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;

import com.hackathon.project.classcontrol.util.Constants;

public final class NotificationIntent {
    private final String classroomId;
    private final String classroomName;
    private final String classroomSubjectName;
    private final String senderName;
    private final String message;
    private final String messageType;
    private final String photoUrl;
    private final long sentTime;

    private NotificationIntent(Builder builder) {
        classroomId = builder.classroomId;
        classroomName = builder.classroomName;
        classroomSubjectName = builder.classroomSubjectName;
        senderName = builder.senderName;
        message = builder.message;
        messageType = builder.messageType;
        photoUrl = builder.photoUrl;
        sentTime = builder.sentTime;
    }

    public NotificationIntent(@NonNull Intent intent) {
        classroomId = intent.getStringExtra(Constants.EXTRA_CLASSROOM_ID);
        classroomName = intent.getStringExtra(Constants.EXTRA_CLASSROOM_NAME);
        classroomSubjectName = intent.getStringExtra(Constants.EXTRA_SUBJECT_NAME);
        senderName = intent.getStringExtra(Constants.EXTRA_SENDER_NAME);
        message = intent.getStringExtra(Constants.EXTRA_MESSAGE);
        messageType = intent.getStringExtra(Constants.EXTRA_MESSAGE_TYPE);
        photoUrl = intent.getStringExtra(Constants.EXTRA_PHOTO_URL);
        sentTime = intent.getLongExtra(Constants.EXTRA_SENT_TIME, -1);
    }

    public String getClassroomId() {
        return classroomId;
    }

    public String getClassroomName() {
        return classroomName;
    }

    public String getClassroomSubjectName() {
        return classroomSubjectName;
    }

    public String getSenderName() {
        return senderName;
    }

    public String getMessage() {
        return message;
    }

    public String getMessageType() {
        return messageType;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public long getSentTime() {
        return sentTime;
    }

    @Override
    public String toString() {
        return "NotificationIntent{" +
                "classroomId='" + classroomId + '\'' +
                ", classroomName='" + classroomName + '\'' +
                ", classroomSubjectName='" + classroomSubjectName + '\'' +
                ", senderName='" + senderName + '\'' +
                ", message='" + message + '\'' +
                ", messageType='" + messageType + '\'' +
                ", photoUrl='" + photoUrl + '\'' +
                ", sentTime=" + sentTime +
                '}';
    }

    public static final class Builder {
        private String classroomId;
        private String classroomName;
        private String classroomSubjectName;
        private String senderName;
        private String message;
        private String messageType;
        private String photoUrl;
        private long sentTime;

        @NonNull
        public Builder setClassroomId(String classroomId) {
            this.classroomId = classroomId;
            return this;
        }

        public Builder setClassroomName(String classroomName) {
            this.classroomName = classroomName;
            return this;
        }

        public Builder setClassroomSubjectName(String classroomSubjectName) {
            this.classroomSubjectName = classroomSubjectName;
            return this;
        }

        @NonNull
        public Builder setSenderName(String senderName) {
            this.senderName = senderName;
            return this;
        }

        @NonNull
        public Builder setMessage(String message) {
            this.message = message;
            return this;
        }

        public Builder setMessageType(String messageType) {
            this.messageType = messageType;
            return this;
        }

        @NonNull
        public Builder getPhotoUrl(String photoUrl) {
            this.photoUrl = photoUrl;
            return this;
        }

        @NonNull
        public Builder setSentTime(long sentTime) {
            this.sentTime = sentTime;
            return this;
        }

        @NonNull
        public Intent buildIntent(@NonNull Context context) {
            Intent intent = new Intent(context, NotificationsReceiver.class);
            intent.putExtra(Constants.EXTRA_CLASSROOM_ID, classroomId);
            intent.putExtra(Constants.EXTRA_CLASSROOM_NAME, classroomName);
            intent.putExtra(Constants.EXTRA_SUBJECT_NAME, classroomSubjectName);
            intent.putExtra(Constants.EXTRA_SENDER_NAME, senderName);
            intent.putExtra(Constants.EXTRA_MESSAGE, message);
            intent.putExtra(Constants.EXTRA_MESSAGE_TYPE, messageType);
            intent.putExtra(Constants.EXTRA_PHOTO_URL, photoUrl);
            intent.putExtra(Constants.EXTRA_SENT_TIME, sentTime);
            return intent;
        }

        @NonNull
        public NotificationIntent buildNotificationIntent() {
            return new NotificationIntent(this);
        }

        @Override
        public String toString() {
            return "Builder{" +
                    "classroomId='" + classroomId + '\'' +
                    ", classroomName='" + classroomName + '\'' +
                    ", classroomSubjectName='" + classroomSubjectName + '\'' +
                    ", senderName='" + senderName + '\'' +
                    ", message='" + message + '\'' +
                    ", messageType='" + messageType + '\'' +
                    ", photoUrl='" + photoUrl + '\'' +
                    ", sentTime=" + sentTime +
                    '}';
        }
    }
}
