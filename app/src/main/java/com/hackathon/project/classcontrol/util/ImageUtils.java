package com.hackathon.project.classcontrol.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Base64;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public final class ImageUtils {

    @NonNull
    public static String bitmapToString(@NonNull Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.WEBP, 50, baos);
        byte[] b = baos.toByteArray();
        return Base64.encodeToString(b, Base64.DEFAULT);
    }

    @Nullable
    public static Bitmap stringToBitmap(@NonNull String encodedString) {
        Bitmap bitmap = null;
        try {
            byte[] encodeByte = Base64.decode(encodedString, Base64.DEFAULT);
            bitmap = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
        } catch (Exception e) {
            Log.e("ERROR", e.getMessage());
        }
        return bitmap;
    }

    @Nullable
    public static String getImagePathFromUri(@NonNull Context context, @Nullable Uri imageUri) {
        String path = null;
        if (imageUri != null) {
            String[] filePath = {MediaStore.Images.Media.DATA};
            try (Cursor cursor = context.getContentResolver().query(imageUri, filePath, null, null, null)) {
                cursor.moveToFirst();
                path = cursor.getString(cursor.getColumnIndex(filePath[0]));
            } catch (Exception e) {
                Log.e("ERROR", e.getMessage());
            }
        }
        return path;
    }

    @NonNull
    public static Uri getUriFromBitmap(@NonNull Context context, @NonNull Bitmap bitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(context.getContentResolver(), bitmap, "Title", null);
        return Uri.parse(path);
    }

    @Nullable
    public static Uri getUriFromBitmapString(@NonNull Context context, @NonNull String bitmapString) {
        Uri uri = null;
        Bitmap bitmap = stringToBitmap(bitmapString);
        if (bitmap != null) {
            uri = getUriFromBitmap(context, bitmap);
        }
        return uri;
    }

    public static File createImageFile(@NonNull Context context) throws IOException {
        @SuppressLint("SimpleDateFormat")
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        return File.createTempFile(imageFileName, ".jpg", storageDir);
    }


    private ImageUtils() {
    }
}
