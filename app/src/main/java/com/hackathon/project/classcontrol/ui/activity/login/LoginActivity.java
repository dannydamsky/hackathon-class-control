package com.hackathon.project.classcontrol.ui.activity.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;

import com.hackathon.project.classcontrol.R;
import com.hackathon.project.classcontrol.data.db.remote.FirebaseAuthHelper;
import com.hackathon.project.classcontrol.data.db.remote.FirebaseDbHelper;
import com.hackathon.project.classcontrol.data.prefs.PreferencesHelper;
import com.hackathon.project.classcontrol.ui.activity.main.MainActivity;
import com.hackathon.project.classcontrol.util.Constants;
import com.shashank.sony.fancytoastlib.FancyToast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public final class LoginActivity extends AppCompatActivity {

    @BindView(R.id.container)
    ConstraintLayout container;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.LoginTheme);
        super.onCreate(savedInstanceState);
        AppCompatDelegate.setDefaultNightMode(PreferencesHelper.getInstance().getCurrentDayNightMode());
        if (!FirebaseAuthHelper.isLoggedIn()) {
            setContentView(R.layout.activity_login);
            ButterKnife.bind(this);
        } else {
            startMainActivity();
        }
    }

    @OnClick(R.id.loginButton)
    public void onLoginPressed() {
        FirebaseAuthHelper.startGoogleLoginIntent(this, Constants.REQUEST_CODE_LOGIN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (FirebaseAuthHelper.isLoggedIn()) {
            showWelcomeToast();
            startMainActivity();
        } else {
            Snackbar.make(container, R.string.error_log_in, Snackbar.LENGTH_LONG).show();
        }
    }

    private void showWelcomeToast() {
        FancyToast.makeText(getApplicationContext(),
                getString(R.string.welcome) + ", " + FirebaseAuthHelper.getDisplayName(),
                FancyToast.LENGTH_SHORT, FancyToast.SUCCESS, false).show();
    }

    private void startMainActivity() {
        FirebaseDbHelper.updateFcmToken();
        MainActivity.start(this);
        finish();
    }
}
