package com.hackathon.project.classcontrol.ui.activity.main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.hackathon.project.classcontrol.R;
import com.hackathon.project.classcontrol.data.db.remote.FirebaseAuthHelper;
import com.hackathon.project.classcontrol.data.db.remote.FirebaseDbHelper;
import com.hackathon.project.classcontrol.data.db.remote.model.User;
import com.hackathon.project.classcontrol.data.prefs.PreferencesHelper;
import com.hackathon.project.classcontrol.ui.activity.chat.ChatActivity;
import com.hackathon.project.classcontrol.ui.activity.creator.CreatorActivity;
import com.hackathon.project.classcontrol.ui.activity.joiner.JoinerActivity;
import com.hackathon.project.classcontrol.ui.activity.main.adapter.ClassroomCardAdapter;
import com.hackathon.project.classcontrol.ui.dialog.SimpleDialog;
import com.hackathon.project.classcontrol.util.ColorUtil;
import com.hackathon.project.classcontrol.util.Constants;
import com.leinardi.android.speeddial.SpeedDialActionItem;
import com.leinardi.android.speeddial.SpeedDialView;

import butterknife.BindView;
import butterknife.ButterKnife;

public final class MainActivity extends AppCompatActivity implements ClassroomCardAdapter.OnClickListener,
        SimpleDialog.Listener {

    public static void start(@NonNull Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        context.startActivity(intent);
    }

    @BindView(R.id.container)
    CoordinatorLayout container;

    @BindView(R.id.addClassFab)
    SpeedDialView addClassFab;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.emptyText)
    TextView emptyText;

    private ClassroomCardAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AppCompatDelegate.setDefaultNightMode(PreferencesHelper.getInstance().getCurrentDayNightMode());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        configureRecyclerView();
        setupSpeedDialViewBehaviour();
        PreferencesHelper.getInstance().setMainActivityRunning(true);
    }

    private void configureRecyclerView() {
        adapter = ClassroomCardAdapter.newInstance(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
        adapter.registerAdapterDataObserver(getAdapterDataObserver());
    }

    private RecyclerView.AdapterDataObserver getAdapterDataObserver() {
        return new RecyclerView.AdapterDataObserver() {
            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                super.onItemRangeInserted(positionStart, itemCount);
                emptyText.setVisibility(View.GONE);
            }

            @Override
            public void onItemRangeRemoved(int positionStart, int itemCount) {
                super.onItemRangeRemoved(positionStart, itemCount);
                if (adapter.getItemCount() == 0) {
                    emptyText.setVisibility(View.VISIBLE);
                }
            }
        };
    }

    private void setupSpeedDialViewBehaviour() {
        addJoinFab();
        addCreateFab();
        setupSpeedDialOnActionSelectedListener();
    }

    private void addJoinFab() {
        addClassFab.addActionItem(
                new SpeedDialActionItem.Builder(R.id.fab_join, R.drawable.ic_plus_one_white_24dp)
                        .setLabel(getString(R.string.join_classroom))
                        .setLabelClickable(true)
                        .create());
    }

    private void addCreateFab() {
        addClassFab.addActionItem(
                new SpeedDialActionItem.Builder(R.id.fab_create, R.drawable.ic_group_add_white_24dp)
                        .setLabel(getString(R.string.create_classroom))
                        .setLabelClickable(true)
                        .setFabBackgroundColor(ColorUtil.getColor(this, R.color.colorTertiary))
                        .create()
        );
    }

    private void setupSpeedDialOnActionSelectedListener() {
        addClassFab.setOnActionSelectedListener(actionItem -> {
            switch (actionItem.getId()) {
                case R.id.fab_create:
                    CreatorActivity.start(this);
                    break;
                case R.id.fab_join:
                    JoinerActivity.start(this);
                    break;
            }
            return false;
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        adapter.stopListening();
    }

    @Override
    protected void onDestroy() {
        PreferencesHelper.getInstance().setMainActivityRunning(false);
        super.onDestroy();
    }

    @Override
    public void onCardClicked(@NonNull User model) {
        ChatActivity.start(this, model.getClassroomId(), model.getClassroomClassName(), model.getClassroomSubjectName());
    }

    @Override
    public void onStudentExitClicked(@NonNull User model) {
        getIntent().putExtra(Constants.EXTRA_IS_STUDENT, true);
        getIntent().putExtra(Constants.EXTRA_CLASSROOM_ID, model.getClassroomId());
        SimpleDialog.show(getSupportFragmentManager(), getString(R.string.leave_classroom),
                getString(R.string.leave_question) + " " + model.getClassroomClassName() + "?");
    }

    @Override
    public void onTeacherShareClicked(@NonNull User model) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.share_message, model.getClassroomClassName(), model.getClassroomPassword()));
        sendIntent.setType("text/plain");
        startActivity(Intent.createChooser(sendIntent, getString(R.string.send_via)));
    }

    @Override
    public void onTeacherDeleteClicked(@NonNull User model) {
        getIntent().putExtra(Constants.EXTRA_IS_STUDENT, false);
        getIntent().putExtra(Constants.EXTRA_CLASSROOM_ID, model.getClassroomId());
        SimpleDialog.show(getSupportFragmentManager(), getString(R.string.delete_classroom),
                getString(R.string.delete_question) + " " + model.getClassroomClassName() + "?" + "\n" +
                        getString(R.string.saved_info_will_be_lost));
    }

    @Override
    public void onSimpleDialogOkPressed() {
        String classroomId = getIntent().getStringExtra(Constants.EXTRA_CLASSROOM_ID);
        if (getIntent().getBooleanExtra(Constants.EXTRA_IS_STUDENT, true)) {
            FirebaseDbHelper.deleteUserFromClassroom(FirebaseAuthHelper.getUserId(), classroomId);
        } else {
            FirebaseDbHelper.deleteClassroom(classroomId);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_day_night:
                toggleDayNightMode();
                break;
        }
        return true;
    }

    private void toggleDayNightMode() {
        int dayNightMode = PreferencesHelper.getInstance().toggleDayNightMode();
        AppCompatDelegate.setDefaultNightMode(dayNightMode);
        recreate();
    }
}
