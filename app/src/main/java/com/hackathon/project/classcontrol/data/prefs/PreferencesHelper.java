package com.hackathon.project.classcontrol.data.prefs;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatDelegate;

public final class PreferencesHelper {
    private static final String RUNTIME_PREFS_NAME =
            "com.hackathon.project.classcontrol.data.prefs.PreferencesHelper.runtime_shared_preferences";

    private static final String RUNTIME_NIGHT_MODE =
            "com.hackathon.project.classcontrol.data.prefs.PreferencesHelper.runtime_night_mode";

    private static final String RUNTIME_USER_FCM_TOKEN =
            "com.hackathon.project.classcontrol.data.prefs.PreferencesHelper.runtime_shared_preferences_user_fcm_token";

    private static final String RUNTIME_CHAT_ACTIVITY_RUNNING =
            "com.hackathon.project.classcontrol.data.prefs.PreferencesHelper.runtime_shared_preferences_chat_activity_running";

    private static final String RUNTIME_CHAT_CLASSROOM =
            "com.hackathon.project.classcontrol.data.prefs.PreferencesHelper.runtime_shared_preferences_chat_classroom";

    private static final String RUNTIME_MAIN_ACTIVITY_RUNNING =
            "com.hackathon.project.classcontrol.data.prefs.PreferencesHelper.runtime_shared_preferences_main_activity_running";

    private static PreferencesHelper instance;

    public static PreferencesHelper getInstance() {
        return instance;
    }

    public static void initialize(@NonNull Context context) {
        instance = new PreferencesHelper(context);
    }

    private final SharedPreferences runtimeSharedPreferences;

    private PreferencesHelper(@NonNull Context context) {
        runtimeSharedPreferences = context.getSharedPreferences(RUNTIME_PREFS_NAME, Context.MODE_PRIVATE);
    }

    public String getUserFcmToken() {
        return runtimeSharedPreferences.getString(RUNTIME_USER_FCM_TOKEN, null);
    }

    public void setUserFcmToken(String fcmToken) {
        runtimeSharedPreferences.edit()
                .putString(RUNTIME_USER_FCM_TOKEN, fcmToken)
                .apply();
    }

    public String chatActivityRunning() {
        boolean isRunning = runtimeSharedPreferences.getBoolean(RUNTIME_CHAT_ACTIVITY_RUNNING, false);
        if (isRunning) {
            return runtimeSharedPreferences.getString(RUNTIME_CHAT_CLASSROOM, null);
        }
        return null;
    }

    public void setChatActivityRunning(String classroomId) {
        runtimeSharedPreferences.edit()
                .putBoolean(RUNTIME_CHAT_ACTIVITY_RUNNING, true)
                .putString(RUNTIME_CHAT_CLASSROOM, classroomId)
                .apply();
    }

    public void setMainActivityRunning(boolean isRunning) {
        runtimeSharedPreferences.edit()
                .putBoolean(RUNTIME_MAIN_ACTIVITY_RUNNING, isRunning)
                .apply();
    }

    public boolean isMainActivityRunning() {
        return runtimeSharedPreferences.getBoolean(RUNTIME_MAIN_ACTIVITY_RUNNING, false);
    }

    public void notifyChatActivityStopped() {
        runtimeSharedPreferences.edit()
                .putBoolean(RUNTIME_CHAT_ACTIVITY_RUNNING, false)
                .apply();
    }

    public int toggleDayNightMode() {
        boolean isEnabled = runtimeSharedPreferences.getBoolean(RUNTIME_NIGHT_MODE, false);
        runtimeSharedPreferences.edit().putBoolean(RUNTIME_NIGHT_MODE, !isEnabled).apply();
        return getNightMode(!isEnabled);
    }

    public int getCurrentDayNightMode() {
        boolean isEnabled = runtimeSharedPreferences.getBoolean(RUNTIME_NIGHT_MODE, false);
        return getNightMode(isEnabled);
    }

    private int getNightMode(boolean isEnabled) {
        if (isEnabled) {
            return AppCompatDelegate.MODE_NIGHT_YES;
        } else {
            return AppCompatDelegate.MODE_NIGHT_NO;
        }
    }
}
