package com.hackathon.project.classcontrol.provider;

import android.content.SearchRecentSuggestionsProvider;

public final class SearchSuggestionsProvider extends SearchRecentSuggestionsProvider {
    public static final String AUTHORITY = "com.hackathon.project.classcontrol.provider.SearchSuggestionsProvider";
    public static final int MODE = DATABASE_MODE_QUERIES;

    public SearchSuggestionsProvider() {
        setupSuggestions(AUTHORITY, MODE);
    }
}
