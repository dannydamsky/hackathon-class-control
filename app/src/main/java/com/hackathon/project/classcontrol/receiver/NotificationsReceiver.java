package com.hackathon.project.classcontrol.receiver;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;

import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.hackathon.project.classcontrol.GlideApp;
import com.hackathon.project.classcontrol.R;
import com.hackathon.project.classcontrol.data.db.remote.MessageType;
import com.hackathon.project.classcontrol.data.prefs.PreferencesHelper;
import com.hackathon.project.classcontrol.ui.activity.chat.ChatActivity;
import com.hackathon.project.classcontrol.util.Constants;

public final class NotificationsReceiver extends BroadcastReceiver {

    private NotificationManager notificationManager;
    private NotificationIntent notificationIntent;

    @Override
    public void onReceive(Context context, Intent intent) {
        notificationIntent = new NotificationIntent(intent);
        if (!isClassroomActive()) {
            setNotificationManager(context);
            createChannel(context);
            buildNotification(context);
        }
    }

    private boolean isClassroomActive() {
        String activeClassroomId = PreferencesHelper.getInstance().chatActivityRunning();
        return activeClassroomId != null &&
                activeClassroomId.equals(notificationIntent.getClassroomId());
    }

    private void setNotificationManager(Context context) {
        if (notificationManager == null) {
            notificationManager = (NotificationManager)
                    context.getSystemService(android.content.Context.NOTIFICATION_SERVICE);
        }
    }

    private void createChannel(@NonNull Context context) {
        if (Constants.IS_OREO_OR_ABOVE) {
            NotificationChannel mChannel = notificationManager.getNotificationChannel(
                    context.getString(R.string.notification_channel_id));
            if (mChannel == null) {
                mChannel = new NotificationChannel(
                        context.getString(R.string.notification_channel_id),
                        context.getString(R.string.notification_channel_desc),
                        NotificationManager.IMPORTANCE_HIGH);

                mChannel.setDescription(context.getString(R.string.notification_channel_desc));
                mChannel.setShowBadge(false);
                mChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);

                notificationManager.createNotificationChannel(mChannel);
            }
        }
    }

    private void buildNotification(Context context) {
        String message = getMessage(context);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context,
                context.getString(R.string.notification_channel_id))
                .setSmallIcon(R.drawable.ic_launcher_white)
                .setAutoCancel(true)
                .setShowWhen(true)
                .setContentIntent(getStartActivityIntent(context))
                .setContentTitle(notificationIntent.getSenderName() + " @ " + notificationIntent.getClassroomName())
                .setContentText(message)
                .setWhen(notificationIntent.getSentTime());
        loadLargeImageIntoNotificationBuilder(context, builder, notificationIntent.getPhotoUrl());
    }

    private String getMessage(Context context) {
        String message;
        MessageType type = MessageType.valueOf(notificationIntent.getMessageType());
        if (type == MessageType.TEXT) {
            message = notificationIntent.getMessage();
        } else {
            message = context.getResources().getString(type.getTypeId());
        }
        return message;
    }

    private PendingIntent getStartActivityIntent(Context context) {
        Intent notificationIntent = new Intent(context, ChatActivity.class);
        notificationIntent.putExtra(Constants.EXTRA_CLASSROOM_ID, this.notificationIntent.getClassroomId());
        notificationIntent.putExtra(Constants.EXTRA_CLASSROOM_NAME, this.notificationIntent.getClassroomName());
        notificationIntent.putExtra(Constants.EXTRA_SUBJECT_NAME, this.notificationIntent.getClassroomSubjectName());
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        return PendingIntent.getActivity(context, 0, notificationIntent, 0);
    }

    private void loadLargeImageIntoNotificationBuilder(Context context, NotificationCompat.Builder builder, String imageUrl) {
        if (imageUrl != null) {
            GlideApp.with(context).asBitmap().circleCrop().load(imageUrl).into(new SimpleTarget<Bitmap>() {
                @Override
                public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                    builder.setLargeIcon(resource);
                    sendNotification(builder.build());
                }

                @Override
                public void onLoadFailed(@Nullable Drawable errorDrawable) {
                    super.onLoadFailed(errorDrawable);
                    setDefaultIcon();
                }

                @Override
                public void onLoadCleared(@Nullable Drawable placeholder) {
                    super.onLoadCleared(placeholder);
                    setDefaultIcon();
                }

                private void setDefaultIcon() {
                    Resources resources = context.getResources();
                    Bitmap bitmap = BitmapFactory.decodeResource(resources, R.drawable.placeholder_user_icon);
                    builder.setLargeIcon(bitmap);
                    sendNotification(builder.build());
                }
            });
        } else {
            Resources resources = context.getResources();
            Bitmap bitmap = BitmapFactory.decodeResource(resources, R.drawable.placeholder_user_icon);
            builder.setLargeIcon(bitmap);
            sendNotification(builder.build());
        }
    }

    private void sendNotification(Notification notification) {
        notificationManager.notify(Constants.REQUEST_CODE_NOTIFICATIONS, notification);
    }
}
