package com.hackathon.project.classcontrol.ui.activity.paint;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.SeekBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.hackathon.project.classcontrol.R;
import com.hackathon.project.classcontrol.data.db.remote.MessageType;
import com.hackathon.project.classcontrol.service.FirebaseSenderService;
import com.hackathon.project.classcontrol.ui.widget.CanvasView;
import com.hackathon.project.classcontrol.util.ColorUtil;
import com.hackathon.project.classcontrol.util.Constants;
import com.hackathon.project.classcontrol.util.metadata.canvas.CanvasMessage;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public final class PaintActivity extends AppCompatActivity implements SeekBar.OnSeekBarChangeListener {

    public static void start(@NonNull Context context, @NonNull String classroomId, @Nullable String messageJson) {
        Intent intent = new Intent(context, PaintActivity.class);
        intent.putExtra(Constants.EXTRA_CLASSROOM_ID, classroomId);
        intent.putExtra(EXTRA_CANVAS_MESSAGE, messageJson);
        context.startActivity(intent);
    }

    private static final String EXTRA_CANVAS_MESSAGE =
            "com.hackathon.project.classcontrol.ui.activity.paint.PaintActivity.EXTRA_CANVAS_MESSAGE";
    private static final String EXTRA_STROKE_WIDTH =
            "com.hackathon.project.classcontrol.ui.activity.paint.PaintActivity.EXTRA_STROKE_WIDTH";
    private static final String EXTRA_STROKE_COLOR =
            "com.hackathon.project.classcontrol.ui.activity.paint.PaintActivity.EXTRA_STROKE_COLOR";

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.canvasView)
    CanvasView canvasView;

    @BindView(R.id.strokeWidthSeekbar)
    SeekBar strokeWidthSeekbar;

    @BindView(R.id.strokeWidthTextView)
    TextView strokeWidthTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paint);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        strokeWidthSeekbar.setOnSeekBarChangeListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        setupCanvasView();
    }

    private void setupCanvasView() {
        String messageJson = getIntent().getStringExtra(EXTRA_CANVAS_MESSAGE);
        int strokeColor = getIntent().getIntExtra(EXTRA_STROKE_COLOR, ColorUtil.getColor(this, R.color.colorBlack));
        int strokeWidth = getIntent().getIntExtra(EXTRA_STROKE_WIDTH, 5);
        strokeWidthSeekbar.setProgress(strokeWidth);
        strokeWidthTextView.setText(getString(R.string.stroke_width) + ": " + strokeWidth + "px");
        if (messageJson != null) {
            CanvasMessage message = new Gson().fromJson(messageJson, CanvasMessage.class);
            canvasView.post(() -> {
                canvasView.setCanvasMessage(message);
                canvasView.setPaintColor(strokeColor);
                canvasView.setPaintWidth(strokeWidth);
            });
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    @Override
    protected void onPause() {
        getIntent().putExtra(EXTRA_CANVAS_MESSAGE, new Gson().toJson(canvasView.getCanvasMessage()));
        getIntent().putExtra(EXTRA_STROKE_COLOR, canvasView.getStrokeColor());
        getIntent().putExtra(EXTRA_STROKE_WIDTH, strokeWidthSeekbar.getProgress());
        super.onPause();
    }

    @OnClick(R.id.eraserButton)
    public void onEraserButtonClicked() {
        canvasView.setEraser();
    }

    @OnClick(R.id.blackButton)
    public void onBlackButtonClicked() {
        canvasView.setPaintColorResource(R.color.colorBlack);
    }

    @OnClick(R.id.deepPurpleButton)
    public void onDeepPurpleButtonClicked() {
        canvasView.setPaintColorResource(R.color.colorDeepPurple);
    }

    @OnClick(R.id.greenButton)
    public void onGreenButtonClicked() {
        canvasView.setPaintColorResource(R.color.colorSecondary);
    }

    @OnClick(R.id.orangeButton)
    public void onOrangeButtonClicked() {
        canvasView.setPaintColorResource(R.color.colorTertiary);
    }

    @OnClick(R.id.sendButton)
    public void onSendButtonClicked() {
        String classroomId = getIntent().getStringExtra(Constants.EXTRA_CLASSROOM_ID);
        String jsonMessage = new Gson().toJson(canvasView.getCanvasMessage());
        FirebaseSenderService.sendNotificationToClassroom(this, classroomId, jsonMessage, MessageType.CANVAS);
        finish();
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        canvasView.setPaintWidth(progress);
        strokeWidthTextView.setText(getString(R.string.stroke_width) + ": " + progress + "px");
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
    }
}
