package com.hackathon.project.classcontrol.data.db.remote;

import android.support.annotation.StringRes;

import com.hackathon.project.classcontrol.R;

public enum MessageType {
    TEXT(R.string.text),
    IMAGE(R.string.image),
    CANVAS(R.string.canvas);

    @StringRes
    int typeId;

    MessageType(@StringRes int typeId) {
        this.typeId = typeId;
    }

    @StringRes
    public int getTypeId() {
        return typeId;
    }
}
