package com.hackathon.project.classcontrol.util.metadata.message;

import com.google.gson.annotations.SerializedName;
import com.hackathon.project.classcontrol.data.db.remote.MessageType;

public final class MetadataPackage {
    @SerializedName("data")
    private MetadataMessage message;
    @SerializedName("to")
    private String receiverToken;

    private MetadataPackage(Builder builder) {
        this.receiverToken = builder.receiverToken;
        this.message = new MetadataMessage();
        this.message.setClassroomId(builder.classroomId);
        this.message.setMessage(builder.message);
        this.message.setMessageType(builder.messageType);
        this.message.setSenderId(builder.senderId);
        this.message.setSentTime(builder.sentTime);
    }

    public MetadataPackage() {
    }

    public MetadataPackage(MetadataMessage message, String receiverToken) {
        this.message = message;
        this.receiverToken = receiverToken;
    }

    public MetadataMessage getMessage() {
        return message;
    }

    public void setMessage(MetadataMessage message) {
        this.message = message;
    }

    public String getReceiverToken() {
        return receiverToken;
    }

    public void setReceiverToken(String receiverToken) {
        this.receiverToken = receiverToken;
    }

    @Override
    public String toString() {
        return "MetadataPackage{" +
                "message=" + message +
                ", receiverToken='" + receiverToken + '\'' +
                '}';
    }

    public static final class Builder {
        private String senderId;
        private String classroomId;
        private String message;
        private String messageType;
        private long sentTime;
        private String receiverToken;

        public Builder setSenderId(String senderId) {
            this.senderId = senderId;
            return this;
        }

        public Builder setClassroomId(String classroomId) {
            this.classroomId = classroomId;
            return this;
        }

        public Builder setMessage(String message) {
            this.message = message;
            return this;
        }

        public Builder setMessageType(MessageType messageType) {
            this.messageType = messageType.name();
            return this;
        }

        public Builder setMessageType(String messageType) {
            this.messageType = messageType;
            return this;
        }

        public Builder setSentTime(long sentTime) {
            this.sentTime = sentTime;
            return this;
        }

        public Builder setReceiverToken(String receiverToken) {
            this.receiverToken = receiverToken;
            return this;
        }

        public MetadataPackage build() {
            return new MetadataPackage(this);
        }

        @Override
        public String toString() {
            return "Builder{" +
                    "senderId='" + senderId + '\'' +
                    ", classroomId='" + classroomId + '\'' +
                    ", message='" + message + '\'' +
                    ", messageType='" + messageType + '\'' +
                    ", sentTime=" + sentTime +
                    ", receiverToken='" + receiverToken + '\'' +
                    '}';
        }
    }
}
