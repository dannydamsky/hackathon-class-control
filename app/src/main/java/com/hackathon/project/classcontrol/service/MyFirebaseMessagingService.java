package com.hackathon.project.classcontrol.service;

import android.content.Intent;
import android.util.Log;

import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.hackathon.project.classcontrol.data.db.remote.FirebaseDbHelper;
import com.hackathon.project.classcontrol.data.db.remote.model.User;
import com.hackathon.project.classcontrol.receiver.NotificationIntent;
import com.hackathon.project.classcontrol.util.metadata.message.MetadataMessage;

import java.util.List;
import java.util.Map;

public final class MyFirebaseMessagingService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        if (remoteMessage != null) {
            Map<String, String> data = remoteMessage.getData();
            MetadataMessage message = MetadataMessage.newInstance(data);
            sendUserToReceiver(message);
        }
    }

    private void sendUserToReceiver(MetadataMessage message) {
        FirebaseDbHelper.getUserFromClassroomByUserIdQuery(message.getSenderId(), message.getClassroomId())
                .get().addOnSuccessListener(queryDocumentSnapshots -> {
            List<DocumentSnapshot> snapshots = queryDocumentSnapshots.getDocuments();
            if (snapshots.size() > 0) {
                User user = queryDocumentSnapshots.getDocuments().get(0).toObject(User.class);
                sendToReceiver(user, message);
            } else {
                Log.e("ERROR", "ERROR IN SENDUSERTORECEIVER");
            }
        }).addOnFailureListener(e -> Log.e("ERROR", e.getMessage()));
    }

    private void sendToReceiver(User user, MetadataMessage message) {
        Intent sendToBroadcast = new NotificationIntent.Builder()
                .setClassroomId(message.getClassroomId())
                .setClassroomName(user.getClassroomClassName())
                .setClassroomSubjectName(user.getClassroomSubjectName())
                .setSenderName(user.getDisplayName())
                .setMessage(message.getMessage())
                .setMessageType(message.getMessageType())
                .getPhotoUrl(user.getPhotoUrl())
                .setSentTime(message.getSentTime())
                .buildIntent(this);
        sendBroadcast(sendToBroadcast);
    }
}
