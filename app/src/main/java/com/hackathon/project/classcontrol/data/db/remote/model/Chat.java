package com.hackathon.project.classcontrol.data.db.remote.model;

import android.support.annotation.NonNull;

import com.google.firebase.firestore.ServerTimestamp;
import com.hackathon.project.classcontrol.data.db.remote.MessageType;

import java.util.Date;

public final class Chat {
    public static final String COLLECTION_NAME = "chats";
    public static final String FIELD_SENDER_ID = "senderId";
    public static final String FIELD_CLASSROOM_ID = "classroomId";
    public static final String FIELD_MESSAGE = "message";
    public static final String FIELD_MESSAGE_TYPE = "messageType";
    public static final String FIELD_SENT_TIME = "sentTime";

    public static Chat newInstance(@NonNull com.hackathon.project.classcontrol.util.metadata.message.MetadataMessage message) {
        Builder builder = new Builder()
                .setSenderId(message.getSenderId())
                .setMessage(message.getMessage())
                .setMessageType(message.getMessageType())
                .setClassroomId(message.getClassroomId())
                .setSentTime(message.getSentTime());
        return builder.build();
    }

    private String senderId;
    private String classroomId;
    private String message;
    private String messageType;
    private Date sentTime;

    private Chat(Builder builder) {
        this.senderId = builder.senderId;
        this.classroomId = builder.classroomId;
        this.message = builder.message;
        this.messageType = builder.messageType;
        this.sentTime = builder.sentTime;
    }

    public Chat() {
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public void setClassroomId(String classroomId) {
        this.classroomId = classroomId;
    }

    public String getClassroomId() {
        return classroomId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public String getMessageType() {
        return messageType;
    }

    @ServerTimestamp
    public Date getSentTime() {
        return sentTime;
    }

    public void setSentTime(Date sentTime) {
        this.sentTime = sentTime;
    }

    @Override
    public String toString() {
        return "Chat Message: " + message + "\n" +
                "Message Type: " + messageType + "\n" +
                "Time Sent: " + sentTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;

        if (!(o instanceof Chat))
            return false;

        Chat chat = (Chat) o;
        return this.classroomId.equals(chat.classroomId) &&
                this.senderId.equals(chat.senderId) &&
                this.message.equals(chat.message) &&
                this.messageType.equals(chat.messageType) &&
                this.sentTime.equals(chat.sentTime);
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + classroomId.hashCode();
        result = 31 * result + senderId.hashCode();
        result = 31 * result + message.hashCode();
        result = 31 * result + messageType.hashCode();
        result = 31 * result + sentTime.hashCode();
        return result;
    }

    public static final class Builder {
        private String senderId;
        private String classroomId;
        private String message;
        private String messageType;
        private Date sentTime;

        public Builder setSenderId(@NonNull String senderId) {
            this.senderId = senderId;
            return this;
        }

        public Builder setClassroomId(@NonNull String classroomId) {
            this.classroomId = classroomId;
            return this;
        }

        public Builder setMessage(@NonNull String message) {
            this.message = message;
            return this;
        }

        public Builder setMessageType(@NonNull MessageType messageType) {
            this.messageType = messageType.name();
            return this;
        }

        public Builder setMessageType(@NonNull String messageType) {
            this.messageType = messageType;
            return this;
        }

        public Builder setSentTime(@NonNull Date sentTime) {
            this.sentTime = sentTime;
            return this;
        }

        public Builder setSentTime(long sentTime) {
            Date date = new Date();
            date.setTime(sentTime);
            this.sentTime = date;
            return this;
        }

        @NonNull
        public Chat build() {
            return new Chat(this);
        }
    }
}
