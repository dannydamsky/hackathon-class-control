package com.hackathon.project.classcontrol.data.db.remote.metadata;

import android.support.annotation.NonNull;

public final class Classroom {
    private String teacherId;
    private String classroomId;
    private String classroomClassName;
    private String classroomSubjectName;
    private String classroomPassword;

    private Classroom(Builder builder) {
        this.teacherId = builder.teacherId;
        this.classroomId = builder.classroomId;
        this.classroomClassName = builder.classroomClassName;
        this.classroomSubjectName = builder.classroomSubjectName;
        this.classroomPassword = builder.classroomPassword;
    }

    public String getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(String teacherId) {
        this.teacherId = teacherId;
    }

    public String getClassroomId() {
        return classroomId;
    }

    public void setClassroomId(String classroomId) {
        this.classroomId = classroomId;
    }

    public String getClassroomClassName() {
        return classroomClassName;
    }

    public void setClassroomClassName(String classroomClassName) {
        this.classroomClassName = classroomClassName;
    }

    public String getClassroomSubjectName() {
        return classroomSubjectName;
    }

    public void setClassroomSubjectName(String classroomSubjectName) {
        this.classroomSubjectName = classroomSubjectName;
    }

    public String getClassroomPassword() {
        return classroomPassword;
    }

    public void setClassroomPassword(String classroomPassword) {
        this.classroomPassword = classroomPassword;
    }

    @Override
    public String toString() {
        return "Class Name: " + classroomClassName + "\n" +
                "Subject Name: " + classroomSubjectName;
    }

    public static final class Builder {
        private String teacherId;
        private String classroomId;
        private String classroomClassName;
        private String classroomSubjectName;
        private String classroomPassword;

        public Builder setTeacherId(@NonNull String teacherId) {
            this.teacherId = teacherId;
            return this;
        }

        public Builder setClassroomClassName(@NonNull String classroomClassName) {
            this.classroomClassName = classroomClassName;
            return this;
        }

        public Builder setClassroomSubjectName(@NonNull String classroomSubjectName) {
            this.classroomSubjectName = classroomSubjectName;
            return this;
        }

        public Builder setClassroomPassword(@NonNull String classroomPassword) {
            this.classroomPassword = classroomPassword;
            return this;
        }

        @NonNull
        public Classroom build() {
            this.classroomId = teacherId + ";" + classroomClassName + ";" + classroomSubjectName;
            return new Classroom(this);
        }
    }
}
