package com.hackathon.project.classcontrol.util;

import android.content.Context;
import android.support.annotation.ColorRes;
import android.support.annotation.NonNull;

public final class ColorUtil {
    public static int getColor(@NonNull Context context, @ColorRes int colorId) {
        if (Constants.IS_MARSHMALLOW_OR_ABOVE)
            return context.getResources().getColor(colorId, null);
        return context.getResources().getColor(colorId);
    }

    private ColorUtil() {
    }
}
