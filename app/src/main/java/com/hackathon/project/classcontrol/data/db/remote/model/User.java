package com.hackathon.project.classcontrol.data.db.remote.model;

import android.support.annotation.NonNull;

import com.hackathon.project.classcontrol.data.db.remote.metadata.Classroom;

public final class User {
    public static final String COLLECTION_NAME = "users";
    public static final String FIELD_ID = "id";
    public static final String FIELD_DISPLAY_NAME = "displayName";
    public static final String FIELD_EMAIL = "email";
    public static final String FIELD_PHONE_NUMBER = "phoneNumber";
    public static final String FIELD_PHOTO_URL = "photoUrl";
    public static final String FIELD_FCM_TOKEN = "fcmToken";
    public static final String FIELD_IS_TEACHER = "teacher";
    public static final String FIELD_CLASSROOM_ID = "classroomId";
    public static final String FIELD_CLASSROOM_CLASS_NAME = "classroomClassName";
    public static final String FIELD_CLASSROOM_SUBJECT_NAME = "classroomSubjectName";
    public static final String FIELD_CLASSROOM_PASSWORD = "classroomPassword";

    private String id;
    private String displayName;
    private String email;
    private String phoneNumber;
    private String photoUrl;
    private String fcmToken;
    private boolean isTeacher;
    private String classroomId;
    private String classroomClassName;
    private String classroomSubjectName;
    private String classroomPassword;

    private User(Builder builder) {
        this.id = builder.id;
        this.displayName = builder.displayName;
        this.email = builder.email;
        this.phoneNumber = builder.phoneNumber;
        this.photoUrl = builder.photoUrl;
        this.fcmToken = builder.fcmToken;
        this.isTeacher = builder.isTeacher;
        this.classroomId = builder.classroomId;
        this.classroomClassName = builder.classroomClassName;
        this.classroomSubjectName = builder.classroomSubjectName;
        this.classroomPassword = builder.classroomPassword;
    }

    public User() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFcmToken() {
        return fcmToken;
    }

    public void setFcmToken(String fcmToken) {
        this.fcmToken = fcmToken;
    }

    public boolean isTeacher() {
        return isTeacher;
    }

    public void setTeacher(boolean teacher) {
        isTeacher = teacher;
    }

    public String getClassroomId() {
        return classroomId;
    }

    public void setClassroomId(String classroomId) {
        this.classroomId = classroomId;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getClassroomSubjectName() {
        return classroomSubjectName;
    }

    public void setClassroomSubjectName(String classroomSubjectName) {
        this.classroomSubjectName = classroomSubjectName;
    }

    public String getClassroomClassName() {
        return classroomClassName;
    }

    public String getClassroomPassword() {
        return classroomPassword;
    }

    public void setClassroomClassName(String classroomClassName) {
        this.classroomClassName = classroomClassName;
    }

    public void setClassroomPassword(String classroomPassword) {
        this.classroomPassword = classroomPassword;
    }

    @Override
    public String toString() {
        return "User Name: " + displayName + "\n" +
                "E-Mail Address: " + email + "\n" +
                "Class Name: " + "\n" +
                "Classroom Subject: " + classroomSubjectName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;

        if (!(o instanceof User))
            return false;

        User user = (User) o;
        return this.id.equals(user.id) &&
                this.classroomId.equals(user.classroomId) &&
                this.isTeacher == user.isTeacher &&
                this.classroomClassName.equals(user.classroomClassName) &&
                this.classroomSubjectName.equals(user.classroomSubjectName);
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + id.hashCode();
        result = 31 * result + classroomId.hashCode();
        result = 31 * result + Boolean.toString(isTeacher).hashCode();
        result = 31 * result + classroomClassName.hashCode();
        result = 31 * result + classroomSubjectName.hashCode();
        return result;
    }

    public static final class Builder {
        private String id;
        private String displayName;
        private String email;
        private String phoneNumber;
        private String photoUrl;
        private String fcmToken;
        private boolean isTeacher;
        private String classroomId;
        private String classroomClassName;
        private String classroomSubjectName;
        private String classroomPassword;

        public Builder setId(@NonNull String id) {
            this.id = id;
            return this;
        }

        public Builder setDisplayName(@NonNull String displayName) {
            this.displayName = displayName;
            return this;
        }

        public Builder setEmail(@NonNull String email) {
            this.email = email;
            return this;
        }

        public Builder setPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
            return this;
        }

        public Builder setPhotoUrl(String photoUrl) {
            this.photoUrl = photoUrl;
            return this;
        }

        public Builder setFcmToken(@NonNull String fcmToken) {
            this.fcmToken = fcmToken;
            return this;
        }

        public Builder setClassroom(@NonNull Classroom classroom) {
            this.isTeacher = this.id.equals(classroom.getTeacherId());
            this.classroomId = classroom.getClassroomId();
            this.classroomClassName = classroom.getClassroomClassName();
            this.classroomSubjectName = classroom.getClassroomSubjectName();
            this.classroomPassword = classroom.getClassroomPassword();
            return this;
        }

        public User build() {
            return new User(this);
        }
    }
}