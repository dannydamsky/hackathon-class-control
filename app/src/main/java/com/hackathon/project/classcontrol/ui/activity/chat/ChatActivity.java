package com.hackathon.project.classcontrol.ui.activity.chat;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.speech.RecognizerIntent;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.hackathon.project.classcontrol.R;
import com.hackathon.project.classcontrol.data.db.remote.MessageType;
import com.hackathon.project.classcontrol.data.db.remote.model.Chat;
import com.hackathon.project.classcontrol.data.prefs.PreferencesHelper;
import com.hackathon.project.classcontrol.service.FirebaseSenderService;
import com.hackathon.project.classcontrol.ui.activity.chat.adapter.ChatAdapter;
import com.hackathon.project.classcontrol.ui.activity.main.MainActivity;
import com.hackathon.project.classcontrol.ui.activity.paint.PaintActivity;
import com.hackathon.project.classcontrol.ui.widget.ChatBox;
import com.hackathon.project.classcontrol.util.Constants;
import com.hackathon.project.classcontrol.util.ImageUtils;
import com.hackathon.project.emoji_library.model.layout.EmojiKeyboardLayout;
import com.hackathon.project.emoji_library.util.SoftKeyboardUtil;

import java.io.File;
import java.io.IOException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public final class ChatActivity extends AppCompatActivity implements ChatAdapter.OnChatClickListener {

    private static final String EXTRA_CURRENT_PHOTO_PATH =
            "com.hackathon.project.classcontrol.ui.activity.chat.ChatActivity.EXTRA_CURRENT_PHOTO_PATH";
    private static final String EXTRA_SELECTED_CHAT_IMAGE =
            "com.hackathon.project.classcontrol.ui.activity.chat.ChatActivity.EXTRA_SELECTED_CHAT_IMAGE";

    public static void start(@NonNull Context context, @NonNull String classroomId, @NonNull String classroomName, @NonNull String subjectName) {
        Intent intent = new Intent(context, ChatActivity.class);
        intent.putExtra(Constants.EXTRA_CLASSROOM_ID, classroomId);
        intent.putExtra(Constants.EXTRA_CLASSROOM_NAME, classroomName);
        intent.putExtra(Constants.EXTRA_SUBJECT_NAME, subjectName);
        context.startActivity(intent);
    }

    @BindView(R.id.container)
    CoordinatorLayout container;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.chatBox)
    ChatBox chatBox;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.emptyText)
    TextView emptyText;

    @BindView(R.id.emojiKeyboard)
    EmojiKeyboardLayout emojiKeyboard;

    private ChatAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AppCompatDelegate.setDefaultNightMode(PreferencesHelper.getInstance().getCurrentDayNightMode());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        ButterKnife.bind(this);
        emojiKeyboard.prepareKeyboard(this, chatBox.getMessageTextBox());
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getIntent().getStringExtra(Constants.EXTRA_CLASSROOM_NAME));
        getSupportActionBar().setSubtitle(getIntent().getStringExtra(Constants.EXTRA_SUBJECT_NAME));
        configureRecyclerView();
        configureOnSendButtonClicked();
        configureVoiceInput();
        configureEmojiBehaviour();
        configureCanvasButton();
        configureGalleryPickButton();
        configureCameraButton();
    }

    private void configureRecyclerView() {
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        adapter = ChatAdapter.newInstance(this, getIntent().getStringExtra(Constants.EXTRA_CLASSROOM_ID));
        recyclerView.setAdapter(adapter);
        adapter.registerAdapterDataObserver(getAdapterDataObserver(linearLayoutManager));
    }

    private RecyclerView.AdapterDataObserver getAdapterDataObserver(LinearLayoutManager linearLayoutManager) {
        return new RecyclerView.AdapterDataObserver() {
            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                super.onItemRangeInserted(positionStart, itemCount);
                linearLayoutManager.scrollToPositionWithOffset(adapter.getItemCount() - 1, 0);
                emptyText.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onItemRangeRemoved(int positionStart, int itemCount) {
                super.onItemRangeRemoved(positionStart, itemCount);
                if (adapter.getItemCount() == 0) {
                    emptyText.setVisibility(View.VISIBLE);
                }
            }
        };
    }

    private void configureOnSendButtonClicked() {
        chatBox.setSendButtonClickListener(str -> {
            if (!str.isEmpty()) {
                String classroomId = getIntent().getStringExtra(Constants.EXTRA_CLASSROOM_ID);
                MessageType messageType = MessageType.TEXT;
                FirebaseSenderService.sendNotificationToClassroom(this, classroomId, str, messageType);
            }
        });
    }

    private void configureCanvasButton() {
        chatBox.setCanvasButtonClickListener(v -> {
            String classroomId = getIntent().getStringExtra(Constants.EXTRA_CLASSROOM_ID);
            PaintActivity.start(this, classroomId, null);
        });
    }

    private void configureGalleryPickButton() {
        chatBox.setGalleryButtonClickListener(v -> dispatchPickPictureIntent());
    }

    private void configureCameraButton() {
        chatBox.setCameraButtonClickListener(v -> dispatchTakePictureIntent());
    }

    private void dispatchPickPictureIntent() {
        if (hasExternalStorageWritingPermissions()) {
            startGalleryChooserIntent();
        } else {
            requestExternalStoragePermissions(Constants.REQUEST_CODE_EXTERNAL_STORAGE_PERMISSIONS_GALLERY);
        }
    }

    private void requestExternalStoragePermissions(int requestCode) {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                requestCode);
    }

    private void startGalleryChooserIntent() {
        Intent intentGetImage = new Intent(Intent.ACTION_GET_CONTENT);
        intentGetImage.setType("image/*");

        Intent intentPicker = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intentPicker.setType("image/*");

        Intent intentChooser = Intent.createChooser(intentGetImage, getString(R.string.choose_gallery));
        intentChooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[]{intentPicker});

        startActivityForResult(intentChooser, Constants.REQUEST_CODE_GALLERY);
    }

    private boolean hasExternalStorageWritingPermissions() {
        int permission = ContextCompat
                .checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        return permission == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        checkForExternalStoragePermissionGranted(requestCode, grantResults);
    }

    private void checkForExternalStoragePermissionGranted(int requestCode, int[] grantResults) {
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            if (requestCode == Constants.REQUEST_CODE_EXTERNAL_STORAGE_PERMISSIONS_GALLERY) {
                dispatchPickPictureIntent();
            } else {
                explicitStartViewImageActivity();
            }
        } else {
            Snackbar.make(container, getString(R.string.error_permissions), Snackbar.LENGTH_LONG).show();
        }
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            File photoFile = getPhotoFile();
            if (photoFile != null) {
                startCameraActivity(takePictureIntent, photoFile);
            } else {
                Snackbar.make(container, R.string.error_camera_open, Snackbar.LENGTH_LONG).show();
            }
        } else {
            Snackbar.make(container, R.string.error_camera_open, Snackbar.LENGTH_LONG).show();
        }
    }

    private File getPhotoFile() {
        try {
            File file = ImageUtils.createImageFile(this);
            getIntent().putExtra(EXTRA_CURRENT_PHOTO_PATH, file.getAbsolutePath());
            return file;
        } catch (IOException e) {
            Log.e("ERROR", e.getMessage());
            return null;
        }
    }

    private void startCameraActivity(Intent takePictureIntent, File photoFile) {
        Uri photoURI = FileProvider.getUriForFile(this,
                "com.hackathon.project.classcontrol.fileprovider",
                photoFile);
        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
        startActivityForResult(takePictureIntent, Constants.REQUEST_CODE_CAMERA);
    }

    private void configureVoiceInput() {
        chatBox.setVoiceInputButtonClickListener(v -> {
            Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
            intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
            startActivityForResult(intent, Constants.REQUEST_CODE_VOICE);
        });
    }

    private void configureEmojiBehaviour() {
        chatBox.setEmojiButtonClickListener(v -> {
            if (emojiKeyboard.getVisibility() == View.GONE) {
                showEmojiKeyboard();
            } else {
                hideEmojiKeyboard();
            }
        });
        chatBox.getMessageTextBox().setOnFocusChangeListener((v, k) -> {
            if (emojiKeyboard.getVisibility() == View.VISIBLE) {
                hideEmojiKeyboard();
            }
        });
    }

    private void showEmojiKeyboard() {
        SoftKeyboardUtil.dismissSoftKeyboard(this, chatBox.getMessageTextBox());
        chatBox.setKeyboardMode();
        emojiKeyboard.setVisibility(View.VISIBLE);
    }

    private void hideEmojiKeyboard() {
        emojiKeyboard.setVisibility(View.GONE);
        chatBox.setEmojiMode();
    }

    @Override
    public void onBackPressed() {
        if (emojiKeyboard.getVisibility() == View.VISIBLE) {
            hideEmojiKeyboard();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case Constants.REQUEST_CODE_VOICE:
                getRecognizerData(resultCode, data);
                break;
            case Constants.REQUEST_CODE_CAMERA:
                sendPhotoPicture();
                break;
            case Constants.REQUEST_CODE_GALLERY:
                sendGalleryPicture(resultCode, data);
                break;
            case Constants.REQUEST_CODE_CANVAS:
                break;
        }
    }

    private void sendPhotoPicture() {
        String mCurrentPhotoPath = getIntent().getStringExtra(EXTRA_CURRENT_PHOTO_PATH);
        Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath);
        if (bitmap != null) {
            FirebaseSenderService.sendNotificationToClassroom(this,
                    getIntent().getStringExtra(Constants.EXTRA_CLASSROOM_ID),
                    mCurrentPhotoPath,
                    MessageType.IMAGE);
        }
    }

    private void sendGalleryPicture(int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            Uri pickedImage = data.getData();
            String imagePath = ImageUtils.getImagePathFromUri(this, pickedImage);
            if (imagePath != null) {
                sendImageMessage(imagePath);
            }
        }
    }

    private void sendImageMessage(String bitmapPath) {
        Bitmap bitmap = BitmapFactory.decodeFile(bitmapPath);
        if (bitmap != null) {
            FirebaseSenderService.sendNotificationToClassroom(this,
                    getIntent().getStringExtra(Constants.EXTRA_CLASSROOM_ID),
                    bitmapPath,
                    MessageType.IMAGE);
        }
    }

    private void getRecognizerData(int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            List<String> textMatchList = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            if (!textMatchList.get(0).contains("search")) {
                chatBox.setText(textMatchList.get(0));
            }
        }
    }

    @Override
    public void finish() {
        if (PreferencesHelper.getInstance().isMainActivityRunning()) {
            super.finish();
        } else {
            super.finish();
            MainActivity.start(this);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        adapter.startListening();
        PreferencesHelper.getInstance().setChatActivityRunning(getIntent().getStringExtra(Constants.EXTRA_CLASSROOM_ID));
    }

    @Override
    protected void onStop() {
        super.onStop();
        adapter.stopListening();
        PreferencesHelper.getInstance().notifyChatActivityStopped();
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    @Override
    public void onViewImageClicked(Chat model) {
        getIntent().putExtra(EXTRA_SELECTED_CHAT_IMAGE, model.getMessage());
        if (hasExternalStorageWritingPermissions()) {
            explicitStartViewImageActivity();
        } else {
            requestExternalStoragePermissions(Constants.REQUEST_CODE_EXTERNAL_STORAGE_PERMISSIONS_OPEN);
        }
    }

    private void explicitStartViewImageActivity() {
        String imageByteCode = getIntent().getStringExtra(EXTRA_SELECTED_CHAT_IMAGE);
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.setData(ImageUtils.getUriFromBitmapString(this, imageByteCode));
        startActivity(intent);
    }

    @Override
    public void onEditCanvasClicked(Chat model) {
        String classroomId = getIntent().getStringExtra(Constants.EXTRA_CLASSROOM_ID);
        PaintActivity.start(this, classroomId, model.getMessage());
    }
}
