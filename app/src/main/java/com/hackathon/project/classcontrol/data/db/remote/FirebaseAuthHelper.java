package com.hackathon.project.classcontrol.data.db.remote;

import android.app.Activity;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.firebase.ui.auth.AuthUI;
import com.google.firebase.auth.FirebaseAuth;

import java.util.Collections;

public final class FirebaseAuthHelper {

    public static boolean isLoggedIn() {
        return FirebaseAuth.getInstance().getCurrentUser() != null;
    }

    public static String getPhoneNumber() {
        return FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber();
    }

    public static String getDisplayName() {
        return FirebaseAuth.getInstance().getCurrentUser().getDisplayName();
    }

    public static String getEmail() {
        return FirebaseAuth.getInstance().getCurrentUser().getEmail();
    }

    public static String getUserId() {
        return FirebaseAuth.getInstance().getCurrentUser().getUid();
    }

    @Nullable
    public static String getPhotoUrl() {
        Uri photoUri = FirebaseAuth.getInstance().getCurrentUser().getPhotoUrl();
        if (photoUri != null) {
            return photoUri.toString();
        }
        return null;
    }

    public static void startGoogleLoginIntent(@NonNull Activity activity, int requestCode) {
        activity.startActivityForResult(AuthUI.getInstance().createSignInIntentBuilder()
                .setIsSmartLockEnabled(true)
                .setAvailableProviders(Collections.singletonList(
                        new AuthUI.IdpConfig.GoogleBuilder().build())).build(), requestCode);
    }

    private FirebaseAuthHelper() {
    }
}
