package com.hackathon.project.classcontrol.ui.activity.joiner.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.Query;
import com.hackathon.project.classcontrol.GlideApp;
import com.hackathon.project.classcontrol.R;
import com.hackathon.project.classcontrol.data.db.remote.FirebaseDbHelper;
import com.hackathon.project.classcontrol.data.db.remote.model.User;

import butterknife.BindView;
import butterknife.ButterKnife;

public final class SearchCardAdapter extends FirestoreRecyclerAdapter<User, SearchCardAdapter.ViewHolder> {

    public static SearchCardAdapter newInstance(@NonNull OnClickListener listener, String searchText, boolean searchByTeacherName) {
        Query query;
        if (searchByTeacherName) {
            query = FirebaseDbHelper.getClassroomSearchQueryByTeacherName(searchText).limit(50);
        } else {
            query = FirebaseDbHelper.getClassroomSearchQuery(searchText).limit(50);
        }
        FirestoreRecyclerOptions<User> options = new FirestoreRecyclerOptions.Builder<User>()
                .setQuery(query, User.class)
                .build();
        return new SearchCardAdapter(options, listener);
    }

    private Context context;
    private OnClickListener listener;

    private SearchCardAdapter(@NonNull FirestoreRecyclerOptions<User> options, @NonNull OnClickListener listener) {
        super(options);
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.row_search, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull User model) {
        configureCardStyle(holder, model);
        holder.classNameTextView.setText(model.getClassroomClassName());
        holder.subjectNameTextView.setText(model.getClassroomSubjectName());
        holder.cardView.setOnClickListener(v -> listener.onCardClicked(model));
    }

    private void configureCardStyle(@NonNull ViewHolder holder, @NonNull User model) {
        GlideApp.with(context)
                .load(model.getPhotoUrl())
                .placeholder(R.drawable.placeholder_user_icon)
                .circleCrop()
                .into(holder.iconUser);
        holder.userModeText.setText(model.getDisplayName());
    }

    static final class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.cardView)
        CardView cardView;
        @BindView(R.id.iconUser)
        ImageView iconUser;
        @BindView(R.id.userModeText)
        TextView userModeText;
        @BindView(R.id.classNameTextView)
        TextView classNameTextView;
        @BindView(R.id.subjectNameTextView)
        TextView subjectNameTextView;

        private ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface OnClickListener {
        void onCardClicked(@NonNull User model);
    }
}
