package com.hackathon.project.classcontrol.util;

import java.util.Calendar;

public final class TimestampUtils {
    public static String convertTimestampToTime(long timestamp) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timestamp);

        StringBuilder builder = new StringBuilder();
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        if (hour < 10)
            builder.append(0);
        builder.append(hour).append(":");
        int minute = calendar.get(Calendar.MINUTE);
        if (minute < 10)
            builder.append(0);
        builder.append(minute);
        return builder.toString();
    }

    private TimestampUtils() {
    }
}
