package com.hackathon.project.classcontrol.ui.activity.joiner;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.SearchRecentSuggestions;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

import com.hackathon.project.classcontrol.R;
import com.hackathon.project.classcontrol.data.db.remote.FirebaseAuthHelper;
import com.hackathon.project.classcontrol.data.db.remote.FirebaseDbHelper;
import com.hackathon.project.classcontrol.data.db.remote.metadata.Classroom;
import com.hackathon.project.classcontrol.data.db.remote.model.User;
import com.hackathon.project.classcontrol.provider.SearchSuggestionsProvider;
import com.hackathon.project.classcontrol.ui.activity.joiner.adapter.SearchCardAdapter;
import com.hackathon.project.classcontrol.ui.dialog.PasswordInputDialog;

import butterknife.BindView;
import butterknife.ButterKnife;

public final class JoinerActivity extends AppCompatActivity implements SearchView.OnQueryTextListener,
        SearchCardAdapter.OnClickListener, PasswordInputDialog.Listener {

    private static final String EXTRA_USER_ID =
            "com.hackathon.project.classcontrol.ui.activity.joiner.JoinerActivity.EXTRA_USER_ID";
    private static final String EXTRA_USER_CLASSROOM_CLASS_NAME =
            "com.hackathon.project.classcontrol.ui.activity.joiner.JoinerActivity.EXTRA_USER_CLASSROOM_CLASS_NAME";
    private static final String EXTRA_USER_CLASSROOM_SUBJECT_NAME =
            "com.hackathon.project.classcontrol.ui.activity.joiner.JoinerActivity.EXTRA_USER_CLASSROOM_SUBJECT_NAME";
    private static final String EXTRA_USER_CLASSROOM_PASSWORD =
            "com.hackathon.project.classcontrol.ui.activity.joiner.JoinerActivity.EXTRA_USER_CLASSROOM_PASSWORD";

    private static final String EXTRA_SEARCH_INPUT =
            "com.hackathon.project.classcontrol.ui.activity.joiner.JoinerActivity.EXTRA_SEARCH_INPUT";

    public static void start(@NonNull Context context) {
        Intent intent = new Intent(context, JoinerActivity.class);
        context.startActivity(intent);
    }

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.emptyText)
    TextView emptyText;

    @BindView(R.id.container)
    CoordinatorLayout container;

    private SearchView searchView;
    private SearchCardAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_joiner);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        configureRecyclerView();
    }

    private void configureRecyclerView() {
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (adapter != null)
            adapter.stopListening();
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    private void handleIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            SearchRecentSuggestions suggestions = new SearchRecentSuggestions(this,
                    SearchSuggestionsProvider.AUTHORITY, SearchSuggestionsProvider.MODE);
            suggestions.saveRecentQuery(query, null);
            searchView.setQuery(query, false);
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        handleIntent(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_joiner, menu);

        SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
        if (searchManager != null) {
            searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
            searchView.setOnQueryTextListener(this);
        }

        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        if (!query.isEmpty()) {
            handleIntent(getIntent());
        }
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (!newText.isEmpty()) {
            if (adapter != null)
                adapter.stopListening();
            adapter = SearchCardAdapter.newInstance(this, newText, false);
            recyclerView.setAdapter(adapter);
            adapter.registerAdapterDataObserver(getAdapterDataObserver());
            adapter.startListening();
            getIntent().putExtra(EXTRA_SEARCH_INPUT, newText);
        }
        return false;
    }

    private RecyclerView.AdapterDataObserver getAdapterDataObserver() {
        return new RecyclerView.AdapterDataObserver() {
            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                super.onItemRangeInserted(positionStart, itemCount);
                emptyText.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onItemRangeRemoved(int positionStart, int itemCount) {
                super.onItemRangeRemoved(positionStart, itemCount);
                if (adapter.getItemCount() == 0) {
                    emptyText.setVisibility(View.VISIBLE);
                }
            }
        };
    }

    @Override
    public void onCardClicked(@NonNull User model) {
        if (!model.getId().equals(FirebaseAuthHelper.getUserId())) {
            storeModel(model);
            PasswordInputDialog.show(getSupportFragmentManager(), getString(R.string.enter_password),
                    getString(R.string.enter_the_password_for) + " " + model.getClassroomClassName());
        } else {
            Snackbar.make(container, R.string.error_attempt_to_join_self, Snackbar.LENGTH_LONG).show();
        }
    }

    private void storeModel(@NonNull User model) {
        getIntent().putExtra(EXTRA_USER_ID, model.getId());
        getIntent().putExtra(EXTRA_USER_CLASSROOM_CLASS_NAME, model.getClassroomClassName());
        getIntent().putExtra(EXTRA_USER_CLASSROOM_SUBJECT_NAME, model.getClassroomSubjectName());
        getIntent().putExtra(EXTRA_USER_CLASSROOM_PASSWORD, model.getClassroomPassword());
    }

    @Override
    public void onInputDialogOkPressed(String text) {
        String userId = getIntent().getStringExtra(EXTRA_USER_ID);
        String userClassroomClassName = getIntent().getStringExtra(EXTRA_USER_CLASSROOM_CLASS_NAME);
        String userClassroomSubjectName = getIntent().getStringExtra(EXTRA_USER_CLASSROOM_SUBJECT_NAME);
        String userClassroomPassword = getIntent().getStringExtra(EXTRA_USER_CLASSROOM_PASSWORD);
        if (!isSuccessfulAddingNewUser(text, userId, userClassroomClassName, userClassroomSubjectName, userClassroomPassword)) {
            Snackbar.make(container, R.string.error_incorrect_password, Snackbar.LENGTH_LONG).show();
        }
    }

    private boolean isSuccessfulAddingNewUser(String text, String userId,
                                              String userClassroomClassName,
                                              String userClassroomSubjectName,
                                              String userClassroomPassword) {
        boolean isValidInput = text.equals(userClassroomPassword);
        if (isValidInput) {
            Classroom classroom = new Classroom.Builder()
                    .setTeacherId(userId)
                    .setClassroomClassName(userClassroomClassName)
                    .setClassroomSubjectName(userClassroomSubjectName)
                    .setClassroomPassword(userClassroomPassword)
                    .build();
            FirebaseDbHelper.addClassroomToCurrentUser(classroom);
            finish();
        }
        return isValidInput;
    }
}
