package com.hackathon.project.classcontrol.util.metadata.message;

import java.util.Map;

public final class MetadataMessage {
    public static MetadataMessage newInstance(Map<String, String> data) {
        MetadataMessage message = new MetadataMessage();
        message.setSenderId(data.get(FIELD_SENDER_ID));
        message.setClassroomId(data.get(FIELD_CLASSROOM_ID));
        message.setMessage(data.get(FIELD_MESSAGE));
        message.setMessageType(data.get(FIELD_MESSAGE_TYPE));
        message.setSentTime(Long.parseLong(data.get(FIELD_SENT_TIME)));
        return message;
    }

    private static final String FIELD_SENDER_ID = "senderId";
    private static final String FIELD_CLASSROOM_ID = "classroomId";
    private static final String FIELD_MESSAGE = "message";
    private static final String FIELD_MESSAGE_TYPE = "messageType";
    private static final String FIELD_SENT_TIME = "sentTime";

    private String senderId;
    private String classroomId;
    private String message;
    private String messageType;
    private long sentTime;

    public MetadataMessage() {
    }

    public MetadataMessage(String senderId, String classroomId, String message, String messageType, long sentTime) {
        this.senderId = senderId;
        this.classroomId = classroomId;
        this.message = message;
        this.messageType = messageType;
        this.sentTime = sentTime;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getClassroomId() {
        return classroomId;
    }

    public void setClassroomId(String classroomId) {
        this.classroomId = classroomId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public long getSentTime() {
        return sentTime;
    }

    public void setSentTime(long sentTime) {
        this.sentTime = sentTime;
    }

    @Override
    public String toString() {
        return "MetadataMessage{" +
                "senderId='" + senderId + '\'' +
                ", classroomId='" + classroomId + '\'' +
                ", message='" + message + '\'' +
                ", messageType='" + messageType + '\'' +
                ", sentTime=" + sentTime +
                '}';
    }
}
