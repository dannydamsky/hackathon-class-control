package com.hackathon.project.classcontrol.ui.widget;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageButton;

import com.hackathon.project.classcontrol.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public final class ChatBox extends ConstraintLayout {
    private static final long ANIMATION_DURATION = 150L;

    @BindView(R.id.chatEmojiButton)
    ImageButton chatEmojiButton;

    @BindView(R.id.canvasButton)
    ImageButton canvasButton;

    @BindView(R.id.galleryButton)
    ImageButton galleryButton;

    @BindView(R.id.cameraButton)
    ImageButton cameraButton;

    @BindView(R.id.messageTextBox)
    EditText messageTextBox;

    @BindView(R.id.voiceInputButton)
    ImageButton voiceInputButton;

    @BindView(R.id.sendButton)
    ImageButton sendButton;

    private Animation scaleDownAnimation;
    private Animation scaleUpAnimation;

    public ChatBox(Context context) {
        super(context);
        bindViews();
    }

    public ChatBox(Context context, AttributeSet attrs) {
        super(context, attrs);
        bindViews();
    }

    public ChatBox(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        bindViews();
    }

    private void bindViews() {
        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
        View thisView = layoutInflater.inflate(R.layout.widget_chat_box, this, true);
        ButterKnife.bind(this, thisView);
        loadAnimations();
        setVisibilityOnTextChange();
    }

    private void loadAnimations() {
        scaleDownAnimation = AnimationUtils.loadAnimation(getContext(), R.anim.scale_down);
        scaleDownAnimation.setDuration(ANIMATION_DURATION);
        scaleUpAnimation = AnimationUtils.loadAnimation(getContext(), R.anim.scale_up);
        scaleUpAnimation.setDuration(ANIMATION_DURATION);
    }

    private void setVisibilityOnTextChange() {
        messageTextBox.addTextChangedListener(new TextWatcher() {
            private boolean textWasEmpty;

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                textWasEmpty = messageTextBox.getText().toString().isEmpty();
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                boolean textIsEmpty = messageTextBox.getText().toString().isEmpty();
                if (textIsEmpty && !textWasEmpty) {
                    hideOneViewAndShowTheOther(sendButton, voiceInputButton);
                } else if (textWasEmpty && !textIsEmpty) {
                    hideOneViewAndShowTheOther(voiceInputButton, sendButton);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    private void hideOneViewAndShowTheOther(View viewToHide, View viewToShow) {
        scaleDownAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                viewToHide.setVisibility(INVISIBLE);
                viewToShow.setVisibility(VISIBLE);
                viewToShow.startAnimation(scaleUpAnimation);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });

        viewToHide.startAnimation(scaleDownAnimation);
    }

    public void setEmojiButtonClickListener(View.OnClickListener listener) {
        this.chatEmojiButton.setOnClickListener(listener);
    }

    public void setCanvasButtonClickListener(View.OnClickListener listener) {
        this.canvasButton.setOnClickListener(listener);
    }

    public void setGalleryButtonClickListener(View.OnClickListener listener) {
        this.galleryButton.setOnClickListener(listener);
    }

    public void setCameraButtonClickListener(View.OnClickListener listener) {
        this.cameraButton.setOnClickListener(listener);
    }

    public void setKeyboardMode() {
        this.chatEmojiButton.setImageResource(R.drawable.ic_keyboard_black_24dp);
    }

    public void setEmojiMode() {
        this.chatEmojiButton.setImageResource(R.drawable.ic_insert_emoticon_black_24dp);
    }

    public void setVoiceInputButtonClickListener(View.OnClickListener listener) {
        this.voiceInputButton.setOnClickListener(listener);
    }

    public void setSendButtonClickListener(OnSendClickListener listener) {
        this.sendButton.setOnClickListener(v -> {
            listener.onSendClicked(messageTextBox.getText().toString().trim());
            messageTextBox.setText("");
        });
    }

    public String getText() {
        return messageTextBox.getText().toString();
    }

    public void setText(String text) {
        messageTextBox.setText(text);
    }

    public EditText getMessageTextBox() {
        return messageTextBox;
    }

    public interface OnSendClickListener {
        void onSendClicked(@NonNull String text);
    }
}
