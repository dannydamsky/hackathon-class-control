package com.hackathon.project.classcontrol.data.db.remote.callback;

public interface FirebaseCallback<T> {
    void onTaskSucceeded(T obj);

    void onTaskFailed(Exception e);
}
