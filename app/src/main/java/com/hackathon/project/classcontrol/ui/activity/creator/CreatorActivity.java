package com.hackathon.project.classcontrol.ui.activity.creator;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;

import com.hackathon.project.classcontrol.R;
import com.hackathon.project.classcontrol.data.db.remote.FirebaseAuthHelper;
import com.hackathon.project.classcontrol.data.db.remote.FirebaseDbHelper;
import com.hackathon.project.classcontrol.data.db.remote.metadata.Classroom;
import com.hackathon.project.classcontrol.util.Constants;
import com.hackathon.project.classcontrol.util.PasswordGenerator;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public final class CreatorActivity extends AppCompatActivity implements SeekBar.OnSeekBarChangeListener {

    private static final String EXTRA_UPPERCASE =
            "com.hackathon.project.classcontrol.ui.activity.creator.CreatorActivity.EXTRA_UPPERCASE";
    private static final String EXTRA_LOWERCASE =
            "com.hackathon.project.classcontrol.ui.activity.creator.CreatorActivity.EXTRA_LOWERCASE";
    private static final String EXTRA_DIGIT =
            "com.hackathon.project.classcontrol.ui.activity.creator.CreatorActivity.EXTRA_DIGIT";
    private static final String EXTRA_PUNCTUATION =
            "com.hackathon.project.classcontrol.ui.activity.creator.CreatorActivity.EXTRA_PUNCTUATION";
    private static final String EXTRA_PROGRESS =
            "com.hackathon.project.classcontrol.ui.activity.creator.CreatorActivity.EXTRA_PROGRESS";

    public static void start(@NonNull Context context) {
        Intent intent = new Intent(context, CreatorActivity.class);
        context.startActivity(intent);
    }

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.fab)
    FloatingActionButton fab;
    @BindView(R.id.classNameEditText)
    EditText classNameEditText;
    @BindView(R.id.subjectNameEditText)
    EditText subjectNameEditText;
    @BindView(R.id.passwordEditText)
    EditText passwordEditText;
    @BindView(R.id.uppercaseCheckbox)
    CheckBox uppercaseCheckbox;
    @BindView(R.id.lowercaseCheckbox)
    CheckBox lowercaseCheckbox;
    @BindView(R.id.digitCheckbox)
    CheckBox digitCheckbox;
    @BindView(R.id.punctuationCheckbox)
    CheckBox punctuationCheckbox;
    @BindView(R.id.lengthSeekbar)
    SeekBar lengthSeekbar;
    @BindView(R.id.lengthDisplayText)
    TextView lengthDisplayText;

    int fabDrawableResource = R.drawable.ic_close_white_24dp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_creator);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        configureUppercaseCheckboxChangeListener();
        configureLowercaseCheckboxChangeListener();
        configureDigitCheckboxChangeListener();
        configurePunctuationCheckboxChangeListener();
        lengthSeekbar.setOnSeekBarChangeListener(this);
        watchTexts();
    }

    private void configureUppercaseCheckboxChangeListener() {
        uppercaseCheckbox.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (!(isChecked || lowercaseCheckbox.isChecked() || digitCheckbox.isChecked() || punctuationCheckbox.isChecked())) {
                buttonView.setChecked(true);
            }
        });
    }

    private void configureLowercaseCheckboxChangeListener() {
        lowercaseCheckbox.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (!(isChecked || uppercaseCheckbox.isChecked() || digitCheckbox.isChecked() || punctuationCheckbox.isChecked())) {
                buttonView.setChecked(true);
            }
        });
    }

    private void configureDigitCheckboxChangeListener() {
        digitCheckbox.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (!(isChecked || lowercaseCheckbox.isChecked() || uppercaseCheckbox.isChecked() || punctuationCheckbox.isChecked())) {
                buttonView.setChecked(true);
            }
        });
    }

    private void configurePunctuationCheckboxChangeListener() {
        punctuationCheckbox.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (!(isChecked || lowercaseCheckbox.isChecked() || digitCheckbox.isChecked() || uppercaseCheckbox.isChecked())) {
                buttonView.setChecked(true);
            }
        });
    }

    @SuppressLint("SetTextI18n")
    private void setCharacterAmount(int chars) {
        lengthDisplayText.setText(chars + " " + getString(R.string.characters));
    }

    private void watchTexts() {
        classNameEditText.addTextChangedListener(getTextWatcher());
        subjectNameEditText.addTextChangedListener(getTextWatcher());
        passwordEditText.addTextChangedListener(getTextWatcher());
    }

    private TextWatcher getTextWatcher() {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                seeIfTextsAreFull();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };
    }

    private void seeIfTextsAreFull() {
        String className = classNameEditText.getText().toString();
        String subjectName = subjectNameEditText.getText().toString();
        String password = passwordEditText.getText().toString();
        if (!(className.isEmpty() || subjectName.isEmpty() || password.isEmpty()) && password.length() >= 8 && password.length() <= 20) {
            if (fabDrawableResource != R.drawable.ic_check_white_24dp) {
                fabDrawableResource = R.drawable.ic_check_white_24dp;
                fab.setImageResource(fabDrawableResource);
            }
        } else if (fabDrawableResource != R.drawable.ic_close_white_24dp) {
            fabDrawableResource = R.drawable.ic_close_white_24dp;
            fab.setImageResource(fabDrawableResource);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    @OnClick(R.id.generateButton)
    public void onGenerateButtonClicked() {
        PasswordGenerator.Builder builder = new PasswordGenerator.Builder()
                .useDigits(digitCheckbox.isChecked())
                .useLower(lowercaseCheckbox.isChecked())
                .usePunctuation(punctuationCheckbox.isChecked())
                .useUpper(uppercaseCheckbox.isChecked());
        String password = builder.build().generate(lengthSeekbar.getProgress() + 8);
        passwordEditText.setText(password);
    }

    @OnClick(R.id.fab)
    public void onFabClicked() {
        if (fabDrawableResource == R.drawable.ic_close_white_24dp) {
            finish();
        } else {
            Classroom classroom = new Classroom.Builder()
                    .setTeacherId(FirebaseAuthHelper.getUserId())
                    .setClassroomClassName(classNameEditText.getText().toString())
                    .setClassroomSubjectName(subjectNameEditText.getText().toString())
                    .setClassroomPassword(passwordEditText.getText().toString())
                    .build();
            checkForDuplicateClasses(classroom);
            finish();
        }
    }

    private void checkForDuplicateClasses(Classroom classroom) {
        FirebaseDbHelper.getUserFromClassroomByUserIdQuery(FirebaseAuthHelper.getUserId(),
                FirebaseAuthHelper.getUserId() + ";" + classroom.getClassroomClassName() + ";" + classroom.getClassroomSubjectName())
                .get().addOnSuccessListener(queryDocumentSnapshots -> {
            if (queryDocumentSnapshots.isEmpty())
                FirebaseDbHelper.addClassroomToCurrentUser(classroom);
        }).addOnFailureListener(e -> Log.e("ERROR", e.getMessage()));
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        setCharacterAmount(progress + 8);
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
    }

    @Override
    protected void onPause() {
        super.onPause();
        passPropertiesToIntent();
    }

    private void passPropertiesToIntent() {
        Intent intent = getIntent();
        intent.putExtra(Constants.EXTRA_CLASSROOM_NAME, classNameEditText.getText().toString());
        intent.putExtra(Constants.EXTRA_SUBJECT_NAME, subjectNameEditText.getText().toString());
        intent.putExtra(Constants.EXTRA_PASSWORD, passwordEditText.getText().toString());
        intent.putExtra(EXTRA_UPPERCASE, uppercaseCheckbox.isChecked());
        intent.putExtra(EXTRA_LOWERCASE, lowercaseCheckbox.isChecked());
        intent.putExtra(EXTRA_DIGIT, digitCheckbox.isChecked());
        intent.putExtra(EXTRA_PUNCTUATION, punctuationCheckbox.isChecked());
        intent.putExtra(EXTRA_PROGRESS, lengthSeekbar.getProgress());
    }

    @Override
    protected void onStart() {
        super.onStart();
        getPropertiesFromIntent();
    }

    private void getPropertiesFromIntent() {
        Intent intent = getIntent();
        classNameEditText.setText(intent.getStringExtra(Constants.EXTRA_CLASSROOM_NAME));
        subjectNameEditText.setText(intent.getStringExtra(Constants.EXTRA_SUBJECT_NAME));
        passwordEditText.setText(intent.getStringExtra(Constants.EXTRA_PASSWORD));
        uppercaseCheckbox.setChecked(intent.getBooleanExtra(EXTRA_UPPERCASE, true));
        lowercaseCheckbox.setChecked(intent.getBooleanExtra(EXTRA_LOWERCASE, true));
        digitCheckbox.setChecked(intent.getBooleanExtra(EXTRA_DIGIT, true));
        punctuationCheckbox.setChecked(intent.getBooleanExtra(EXTRA_PUNCTUATION, true));
        int progress = intent.getIntExtra(EXTRA_PROGRESS, 0);
        lengthSeekbar.setProgress(progress);
        setCharacterAmount(progress + 8);
    }
}
