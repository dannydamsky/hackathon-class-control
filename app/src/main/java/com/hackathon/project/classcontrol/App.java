package com.hackathon.project.classcontrol;

import android.app.Application;

import com.hackathon.project.classcontrol.data.prefs.PreferencesHelper;

public final class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        PreferencesHelper.initialize(this);
    }
}
