package com.hackathon.project.classcontrol.ui.activity.main.adapter;

import android.content.Context;
import android.support.annotation.MenuRes;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.Query;
import com.hackathon.project.classcontrol.GlideApp;
import com.hackathon.project.classcontrol.R;
import com.hackathon.project.classcontrol.data.db.remote.FirebaseAuthHelper;
import com.hackathon.project.classcontrol.data.db.remote.FirebaseDbHelper;
import com.hackathon.project.classcontrol.data.db.remote.model.User;
import com.hackathon.project.classcontrol.util.ColorUtil;

import butterknife.BindView;
import butterknife.ButterKnife;

public final class ClassroomCardAdapter extends FirestoreRecyclerAdapter<User, ClassroomCardAdapter.ViewHolder> {

    public static ClassroomCardAdapter newInstance(@NonNull OnClickListener listener) {
        String userId = FirebaseAuthHelper.getUserId();
        Query query = FirebaseDbHelper.getUserClassesQuery(userId);
        FirestoreRecyclerOptions<User> options = new FirestoreRecyclerOptions.Builder<User>()
                .setQuery(query, User.class)
                .build();
        return new ClassroomCardAdapter(options, listener);
    }

    private Context context;
    private OnClickListener listener;

    private ClassroomCardAdapter(@NonNull FirestoreRecyclerOptions<User> options, @NonNull OnClickListener listener) {
        super(options);
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.row_classroom, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull User model) {
        if (model.isTeacher()) {
            configureTeacherCardStyle(holder, model.getPhotoUrl());
            registerOptionsButtonClickForTeacher(holder, model);
        } else {
            configureStudentCardStyle(holder);
            registerOptionsButtonClickForStudent(holder, model);
        }
        holder.classNameTextView.setText(model.getClassroomClassName());
        holder.subjectNameTextView.setText(model.getClassroomSubjectName());
        holder.cardView.setOnClickListener(v -> listener.onCardClicked(model));
    }

    private void configureTeacherCardStyle(@NonNull ViewHolder holder, @NonNull String photoUrl) {
        holder.cardView.setCardBackgroundColor(ColorUtil.getColor(context, R.color.colorSecondary));
        GlideApp.with(context)
                .load(photoUrl)
                .placeholder(R.drawable.placeholder_user_icon)
                .circleCrop()
                .into(holder.iconUser);
        holder.userModeText.setText(R.string.teacher);
    }

    private void configureStudentCardStyle(@NonNull ViewHolder holder) {
        holder.cardView.setCardBackgroundColor(ColorUtil.getColor(context, R.color.colorTertiary));
        holder.iconUser.setImageResource(R.drawable.ic_student_icon);
        holder.userModeText.setText(R.string.student);
    }

    private void registerOptionsButtonClickForTeacher(@NonNull ViewHolder holder, @NonNull User model) {
        registerOptionsButtonClick(holder, R.menu.menu_classroom_card_teacher, item -> {
            switch (item.getItemId()) {
                case R.id.action_share:
                    listener.onTeacherShareClicked(model);
                    break;
                case R.id.action_delete:
                    listener.onTeacherDeleteClicked(model);
                    break;
            }
            return false;
        });
    }

    private void registerOptionsButtonClickForStudent(@NonNull ViewHolder holder, @NonNull User model) {
        registerOptionsButtonClick(holder, R.menu.menu_classroom_card_student, item -> {
            switch (item.getItemId()) {
                case R.id.action_exit:
                    listener.onStudentExitClicked(model);
                    break;
            }
            return false;
        });
    }

    private void registerOptionsButtonClick(@NonNull ViewHolder holder, @MenuRes int menuId, PopupMenu.OnMenuItemClickListener listener) {
        holder.optionsButton.setOnClickListener(v -> {
            PopupMenu menu = new PopupMenu(context, holder.optionsButton);
            menu.inflate(menuId);
            menu.setOnMenuItemClickListener(listener);
            menu.show();
        });
    }

    static final class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.cardView)
        CardView cardView;
        @BindView(R.id.iconUser)
        ImageView iconUser;
        @BindView(R.id.optionsButton)
        ImageButton optionsButton;
        @BindView(R.id.userModeText)
        TextView userModeText;
        @BindView(R.id.classNameTextView)
        TextView classNameTextView;
        @BindView(R.id.subjectNameTextView)
        TextView subjectNameTextView;

        private ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface OnClickListener {
        void onCardClicked(@NonNull User model);

        void onStudentExitClicked(@NonNull User model);

        void onTeacherShareClicked(@NonNull User model);

        void onTeacherDeleteClicked(@NonNull User model);
    }
}
