package com.hackathon.project.classcontrol.util;

import android.os.Build;

public final class Constants {

    // Runtime information:
    public static final boolean IS_OREO_OR_ABOVE = Build.VERSION.SDK_INT >= Build.VERSION_CODES.O;
    public static final boolean IS_MARSHMALLOW_OR_ABOVE = Build.VERSION.SDK_INT >= Build.VERSION_CODES.M;

    // Request codes:
    public static final int REQUEST_CODE_NOTIFICATIONS = 1415;
    public static final int REQUEST_CODE_LOGIN = 2315;
    public static final int REQUEST_CODE_VOICE = 1927;
    public static final int REQUEST_CODE_CAMERA = 1045;
    public static final int REQUEST_CODE_GALLERY = 1752;
    public static final int REQUEST_CODE_CANVAS = 882;
    public static final int REQUEST_CODE_EXTERNAL_STORAGE_PERMISSIONS_GALLERY = 2831;
    public static final int REQUEST_CODE_EXTERNAL_STORAGE_PERMISSIONS_OPEN = 528;

    // Dialog Bundle Keys:
    public static final String DIALOG_MESSAGE = "dialog_message";
    public static final String DIALOG_TITLE = "dialog_title";

    // Extras:
    public static final String EXTRA_CLASSROOM_ID = "EXTRA_CLASSROOM_ID";
    public static final String EXTRA_CLASSROOM_NAME = "EXTRA_CLASSROOM_NAME";
    public static final String EXTRA_SUBJECT_NAME = "EXTRA_SUBJECT_NAME";
    public static final String EXTRA_PASSWORD = "EXTRA_CLASSROOM_PASSWORD";
    public static final String EXTRA_SENDER_NAME = "EXTRA_SENDER_NAME";
    public static final String EXTRA_MESSAGE = "EXTRA_MESSAGE";
    public static final String EXTRA_MESSAGE_TYPE = "EXTRA_MESSAGE_TYPE";
    public static final String EXTRA_SENT_TIME = "EXTRA_SENT_TIME";
    public static final String EXTRA_PHOTO_URL = "EXTRA_PHOTO_URL";
    public static final String EXTRA_IS_STUDENT = "EXTRA_IS_STUDENT";

    private Constants() {
    }
}
