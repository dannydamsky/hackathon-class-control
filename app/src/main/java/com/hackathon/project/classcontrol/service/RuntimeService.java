package com.hackathon.project.classcontrol.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.hackathon.project.classcontrol.data.prefs.PreferencesHelper;

public final class RuntimeService extends Service {

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
        PreferencesHelper.getInstance().setMainActivityRunning(false);
    }
}
