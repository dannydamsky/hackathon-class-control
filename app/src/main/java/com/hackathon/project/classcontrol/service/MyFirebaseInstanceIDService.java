package com.hackathon.project.classcontrol.service;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.hackathon.project.classcontrol.data.prefs.PreferencesHelper;

public final class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {
    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        storeNewTokenInPreferences();
    }

    private void storeNewTokenInPreferences() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        PreferencesHelper.getInstance().setUserFcmToken(refreshedToken);
    }
}
