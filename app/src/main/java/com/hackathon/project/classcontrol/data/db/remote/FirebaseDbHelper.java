package com.hackathon.project.classcontrol.data.db.remote;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.hackathon.project.classcontrol.data.db.remote.callback.FirebaseCallback;
import com.hackathon.project.classcontrol.data.db.remote.metadata.Classroom;
import com.hackathon.project.classcontrol.data.db.remote.model.Chat;
import com.hackathon.project.classcontrol.data.db.remote.model.User;
import com.hackathon.project.classcontrol.data.prefs.PreferencesHelper;

public final class FirebaseDbHelper {

    public static void updateFcmToken() {
        FirebaseFirestore.getInstance()
                .collection(com.hackathon.project.classcontrol.data.db.remote.model.User.COLLECTION_NAME)
                .whereEqualTo(com.hackathon.project.classcontrol.data.db.remote.model.User.FIELD_ID, FirebaseAuthHelper.getUserId())
                .get().addOnSuccessListener(FirebaseDbHelper::updateUsersFromSnapshots);
    }

    private static void updateUsersFromSnapshots(QuerySnapshot queryDocumentSnapshots) {
        String fcmToken = PreferencesHelper.getInstance().getUserFcmToken();
        for (DocumentSnapshot document : queryDocumentSnapshots.getDocuments()) {
            User user = document.toObject(User.class);
            if (user != null) {
                user.setFcmToken(fcmToken);
                addOrReplaceUser(user);
            }
        }
    }

    @NonNull
    public static Query getClassroomChatsQuery(@NonNull String classroomId) {
        return FirebaseFirestore.getInstance()
                .collection(Chat.COLLECTION_NAME)
                .whereEqualTo(Chat.FIELD_CLASSROOM_ID, classroomId)
                .orderBy(Chat.FIELD_SENT_TIME);
    }

    @NonNull
    public static Query getUserClassesQuery(@NonNull String userId) {
        return FirebaseFirestore.getInstance()
                .collection(com.hackathon.project.classcontrol.data.db.remote.model.User.COLLECTION_NAME)
                .whereEqualTo(com.hackathon.project.classcontrol.data.db.remote.model.User.FIELD_ID, userId)
                .orderBy(com.hackathon.project.classcontrol.data.db.remote.model.User.FIELD_CLASSROOM_SUBJECT_NAME);
    }

    @NonNull
    public static Query getClassroomUsersQuery(@NonNull String classroomId) {
        return FirebaseFirestore.getInstance()
                .collection(com.hackathon.project.classcontrol.data.db.remote.model.User.COLLECTION_NAME)
                .whereEqualTo(com.hackathon.project.classcontrol.data.db.remote.model.User.FIELD_CLASSROOM_ID, classroomId)
                .orderBy(com.hackathon.project.classcontrol.data.db.remote.model.User.FIELD_DISPLAY_NAME);
    }

    @NonNull
    public static Query getClassroomsQuery() {
        return FirebaseFirestore.getInstance()
                .collection(com.hackathon.project.classcontrol.data.db.remote.model.User.COLLECTION_NAME)
                .whereEqualTo(com.hackathon.project.classcontrol.data.db.remote.model.User.FIELD_IS_TEACHER, true)
                .orderBy(com.hackathon.project.classcontrol.data.db.remote.model.User.FIELD_CLASSROOM_SUBJECT_NAME);
    }

    @NonNull
    public static Query getClassroomSearchQuery(@NonNull String searchText) {
        return FirebaseFirestore.getInstance()
                .collection(com.hackathon.project.classcontrol.data.db.remote.model.User.COLLECTION_NAME)
                .whereEqualTo(com.hackathon.project.classcontrol.data.db.remote.model.User.FIELD_IS_TEACHER, true)
                .orderBy(com.hackathon.project.classcontrol.data.db.remote.model.User.FIELD_CLASSROOM_CLASS_NAME)
                .startAt(searchText)
                .endAt(searchText + "\uf8ff");
    }

    @NonNull
    public static Query getClassroomSearchQueryByTeacherName(@NonNull String searchText) {
        return FirebaseFirestore.getInstance()
                .collection(com.hackathon.project.classcontrol.data.db.remote.model.User.COLLECTION_NAME)
                .whereEqualTo(com.hackathon.project.classcontrol.data.db.remote.model.User.FIELD_IS_TEACHER, true)
                .orderBy(com.hackathon.project.classcontrol.data.db.remote.model.User.FIELD_DISPLAY_NAME)
                .startAt(searchText)
                .endAt(searchText + "\uf8ff");
    }

    @NonNull
    public static Query getUserFromClassroomByFcmTokenQuery(@NonNull String fcmToken, @NonNull String classroomId) {
        return FirebaseFirestore.getInstance()
                .collection(com.hackathon.project.classcontrol.data.db.remote.model.User.COLLECTION_NAME)
                .whereEqualTo(com.hackathon.project.classcontrol.data.db.remote.model.User.FIELD_FCM_TOKEN, fcmToken)
                .whereEqualTo(com.hackathon.project.classcontrol.data.db.remote.model.User.FIELD_CLASSROOM_ID, classroomId);
    }

    @NonNull
    public static Query getUserFromClassroomByUserIdQuery(@NonNull String userId, @NonNull String classroomId) {
        return FirebaseFirestore.getInstance()
                .collection(com.hackathon.project.classcontrol.data.db.remote.model.User.COLLECTION_NAME)
                .whereEqualTo(com.hackathon.project.classcontrol.data.db.remote.model.User.FIELD_ID, userId)
                .whereEqualTo(com.hackathon.project.classcontrol.data.db.remote.model.User.FIELD_CLASSROOM_ID, classroomId);
    }

    @NonNull
    public static Query getTeachersQuery() {
        return FirebaseFirestore.getInstance()
                .collection(com.hackathon.project.classcontrol.data.db.remote.model.User.COLLECTION_NAME)
                .whereEqualTo(com.hackathon.project.classcontrol.data.db.remote.model.User.FIELD_IS_TEACHER, true)
                .orderBy(com.hackathon.project.classcontrol.data.db.remote.model.User.FIELD_DISPLAY_NAME);
    }

    @NonNull
    public static Query getStudentsQuery() {
        return FirebaseFirestore.getInstance()
                .collection(com.hackathon.project.classcontrol.data.db.remote.model.User.COLLECTION_NAME)
                .whereEqualTo(com.hackathon.project.classcontrol.data.db.remote.model.User.FIELD_IS_TEACHER, false)
                .orderBy(com.hackathon.project.classcontrol.data.db.remote.model.User.FIELD_DISPLAY_NAME);
    }

    @NonNull
    public static Query getClassroomTeacherQuery(@NonNull String classroomId) {
        return FirebaseFirestore.getInstance()
                .collection(com.hackathon.project.classcontrol.data.db.remote.model.User.COLLECTION_NAME)
                .whereEqualTo(com.hackathon.project.classcontrol.data.db.remote.model.User.FIELD_CLASSROOM_ID, classroomId)
                .whereEqualTo(com.hackathon.project.classcontrol.data.db.remote.model.User.FIELD_IS_TEACHER, true);
    }

    public static void addChat(@NonNull Chat chat) {
        FirebaseFirestore.getInstance().collection(Chat.COLLECTION_NAME).add(chat);
    }

    public static User addClassroomToCurrentUser(@NonNull Classroom classroom) {
        User user = new User.Builder()
                .setId(FirebaseAuthHelper.getUserId())
                .setDisplayName(FirebaseAuthHelper.getDisplayName())
                .setEmail(FirebaseAuthHelper.getEmail())
                .setFcmToken(PreferencesHelper.getInstance().getUserFcmToken())
                .setPhoneNumber(FirebaseAuthHelper.getPhoneNumber())
                .setPhotoUrl(FirebaseAuthHelper.getPhotoUrl())
                .setClassroom(classroom)
                .build();
        addOrReplaceUser(user);
        return user;
    }

    private static void addOrReplaceUser(@NonNull User user) {
        FirebaseFirestore.getInstance().collection(com.hackathon.project.classcontrol.data.db.remote.model.User.COLLECTION_NAME)
                .document(user.getId() + ";" + user.getClassroomId()).set(user);
    }

    public static void getUser(@NonNull FirebaseCallback<User> callback, @NonNull String userId, @NonNull String classroomId) {
        Query userQuery = getUserFromClassroomByUserIdQuery(userId, classroomId);
        getFirstUserFromQuery(callback, userQuery);
    }

    public static void getClassTeacher(@NonNull FirebaseCallback<User> callback, @NonNull String classroomId) {
        Query teacherQuery = getClassroomTeacherQuery(classroomId);
        getFirstUserFromQuery(callback, teacherQuery);
    }

    private static void getFirstUserFromQuery(FirebaseCallback<User> callback, Query query) {
        query.get().addOnSuccessListener(queryDocumentSnapshots -> {
            DocumentSnapshot firstDoc = queryDocumentSnapshots.getDocuments().get(0);
            User user = firstDoc.toObject(User.class);
            callback.onTaskSucceeded(user);
        }).addOnFailureListener(callback::onTaskFailed);
    }

    public static void deleteUserFromClassroom(@NonNull String userId, @NonNull String classroomId) {
        FirebaseFirestore.getInstance().collection(com.hackathon.project.classcontrol.data.db.remote.model.User.COLLECTION_NAME)
                .document(userId + ";" + classroomId)
                .delete();
    }

    public static void deleteClassroom(@NonNull String classroomId) {
        deleteClassroomChats(classroomId);
        getClassroomUsersQuery(classroomId).get().addOnSuccessListener(queryDocumentSnapshots -> {
            for (DocumentSnapshot snapshot : queryDocumentSnapshots) {
                FirebaseFirestore.getInstance().collection(com.hackathon.project.classcontrol.data.db.remote.model.User.COLLECTION_NAME)
                        .document(snapshot.getId()).delete();
            }
        }).addOnFailureListener(e -> Log.e("ERROR", e.getMessage()));
    }

    private static void deleteClassroomChats(@NonNull String classroomId) {
        getClassroomChatsQuery(classroomId).get().addOnSuccessListener(queryDocumentSnapshots -> {
            for (DocumentSnapshot snapshot : queryDocumentSnapshots) {
                FirebaseFirestore.getInstance().collection(Chat.COLLECTION_NAME)
                        .document(snapshot.getId()).delete();
            }
        }).addOnFailureListener(e -> Log.e("ERROR", e.getMessage()));
    }

    private FirebaseDbHelper() {
    }
}
