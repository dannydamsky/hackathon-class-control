package com.hackathon.project.classcontrol.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.firebase.firestore.DocumentSnapshot;
import com.google.gson.Gson;
import com.hackathon.project.classcontrol.R;
import com.hackathon.project.classcontrol.data.db.remote.FirebaseAuthHelper;
import com.hackathon.project.classcontrol.data.db.remote.FirebaseDbHelper;
import com.hackathon.project.classcontrol.data.db.remote.MessageType;
import com.hackathon.project.classcontrol.data.db.remote.model.Chat;
import com.hackathon.project.classcontrol.data.db.remote.model.User;
import com.hackathon.project.classcontrol.data.prefs.PreferencesHelper;
import com.hackathon.project.classcontrol.util.ImageUtils;
import com.hackathon.project.classcontrol.util.metadata.message.MetadataMessage;
import com.hackathon.project.classcontrol.util.metadata.message.MetadataPackage;
import com.shashank.sony.fancytoastlib.FancyToast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Calendar;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public final class FirebaseSenderService extends Service {

    private static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    private static final String FCM_MESSAGE_URL = "https://fcm.googleapis.com/fcm/send";
    private static final String SERVER_KEY =
            "AAAA2twCxcM:APA91bHU7oyOJAEMmqGuZyJcQMFxr_v572_SdFvOtre8oNqiplr5-zIPaWioyBwpWtiFRZG1ygX7orscXGxAVngP_UGII3K3c1m8FlG_BEVSP6OcAK-U6ho2imxlR6nnTfIp189xVZ1B";
    private static final String SEND_MESSAGE =
            "com.hackathon.project.classcontrol.service.FirebaseSenderService.SEND_MESSAGE";
    private static final String SEND_CLASSROOM_ID =
            "com.hackathon.project.classcontrol.service.FirebaseSenderService.SEND_CLASSROOM_ID";
    private static final String SEND_MESSAGE_TYPE =
            "com.hackathon.project.classcontrol.service.FirebaseSenderService.SEND_MESSAGE_TYPE";

    public static void sendNotificationToClassroom(@NonNull Context context,
                                                   @NonNull String classroomId,
                                                   @NonNull String message,
                                                   @NonNull MessageType messageType) {
        Intent intent = new Intent(context, FirebaseSenderService.class);
        intent.putExtra(SEND_CLASSROOM_ID, classroomId);
        intent.putExtra(SEND_MESSAGE, message);
        intent.putExtra(SEND_MESSAGE_TYPE, messageType.name());
        context.startService(intent);
    }

    private OkHttpClient okHttpClient;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        okHttpClient = new OkHttpClient();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null)
            new MessagingThread(intent).start();
        return START_STICKY;
    }

    private final class MessagingThread extends Thread {
        private Intent intent;
        private MetadataPackage.Builder builder;

        private MessagingThread(Intent intent) {
            super();
            this.intent = intent;
            this.builder = new MetadataPackage.Builder();
        }

        @Override
        public void run() {
            handleIntent();
            saveChatToFirebaseDb();
            handleSendingToClassroom();
        }

        private void handleIntent() {
            builder.setClassroomId(intent.getStringExtra(SEND_CLASSROOM_ID))
                    .setMessage(intent.getStringExtra(SEND_MESSAGE))
                    .setMessageType(intent.getStringExtra(SEND_MESSAGE_TYPE))
                    .setSentTime(Calendar.getInstance().getTimeInMillis())
                    .setSenderId(FirebaseAuthHelper.getUserId());
        }

        private void saveChatToFirebaseDb() {
            MetadataMessage metadataMessage = builder.build().getMessage();
            if (metadataMessage.getMessageType().equals(MessageType.IMAGE.name())) {
                Bitmap bitmap = BitmapFactory.decodeFile(metadataMessage.getMessage());
                metadataMessage.setMessage(ImageUtils.bitmapToString(bitmap));
            }
            Chat chat = Chat.newInstance(metadataMessage);
            FirebaseDbHelper.addChat(chat);
        }

        private void handleSendingToClassroom() {
            MetadataPackage metadataPackage = builder.build();
            if (!metadataPackage.getMessage().getMessageType().equals(MessageType.TEXT.name())) {
                metadataPackage.getMessage().setMessage("");
            }
            FirebaseDbHelper.getClassroomUsersQuery(metadataPackage.getMessage().getClassroomId()).get()
                    .addOnSuccessListener(queryDocumentSnapshots -> {
                        for (DocumentSnapshot document : queryDocumentSnapshots) {
                            String targetFcmToken = document.getString(User.FIELD_FCM_TOKEN);
                            if (!targetFcmToken.equals(PreferencesHelper.getInstance().getUserFcmToken())) {
                                metadataPackage.setReceiverToken(targetFcmToken);
                                PostThread thread = new PostThread(metadataPackage);
                                thread.start();
                            }
                        }
                    }).addOnFailureListener(e -> {
                Log.e("ERROR", e.getMessage());
                showFancyToastError();
            });
        }

        private void showFancyToastError() {
            FancyToast.makeText(getApplicationContext(),
                    getString(R.string.error_sending_notification),
                    FancyToast.LENGTH_LONG, FancyToast.ERROR, false).show();
        }
    }

    private final class PostThread extends Thread {
        private MetadataPackage metadataPackage;

        private PostThread(MetadataPackage metadataPackage) {
            this.metadataPackage = metadataPackage;
        }

        @Override
        public void run() {
            super.run();
            generateGsonToSend();
        }

        private void generateGsonToSend() {
            String result = initSendTask(new Gson().toJson(metadataPackage));
            notifyOfSuccessOrFailure(result);
        }

        private String initSendTask(String gson) {
            try {
                return postToFCM(gson);
            } catch (Exception e) {
                Log.e("ERROR", e.getMessage());
                showFancyToastError();
                return null;
            }
        }

        private String postToFCM(String bodyString) throws IOException {
            Request request = buildRequest(bodyString);
            Response response = okHttpClient.newCall(request).execute();
            String resultJson = response.body().string();
            response.close();
            return resultJson;
        }

        private Request buildRequest(String bodyString) {
            RequestBody body = RequestBody.create(JSON, bodyString);
            return new Request.Builder()
                    .url(FCM_MESSAGE_URL)
                    .post(body)
                    .addHeader("Content-Type", "application/json")
                    .addHeader("Authorization", "key=" + SERVER_KEY)
                    .build();
        }

        private void notifyOfSuccessOrFailure(String data) {
            try {
                parseResultJson(data);
            } catch (Exception e) {
                Log.e("ERROR", e.getMessage());
                showFancyToastError();
            }
        }

        private void parseResultJson(String json) throws JSONException {
            if (json != null) {
                JSONObject resultJson = new JSONObject(json);
                int success = resultJson.getInt("success");
                int failure = resultJson.getInt("failure");

                Log.d("SendSuccess", "Success = " + success);
                Log.d("SendFail", "Failure = " + failure);

                if (failure > 0) {
                    showFancyToastError();
                }
            } else {
                showFancyToastError();
            }
        }

        private void showFancyToastError() {
            FancyToast.makeText(getApplicationContext(),
                    getString(R.string.error_sending_notification),
                    FancyToast.LENGTH_LONG, FancyToast.ERROR, false).show();
        }
    }
}
