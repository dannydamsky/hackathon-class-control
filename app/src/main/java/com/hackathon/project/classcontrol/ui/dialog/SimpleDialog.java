package com.hackathon.project.classcontrol.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;

import com.hackathon.project.classcontrol.R;
import com.hackathon.project.classcontrol.util.Constants;

public final class SimpleDialog extends DialogFragment {

    public static final String TAG = "simple_alert_dialog";

    public static void show(@NonNull FragmentManager fragmentManager, @NonNull String title, @NonNull String message) {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Fragment previousDialog = fragmentManager.findFragmentByTag(TAG);

        if (previousDialog != null)
            fragmentTransaction.remove(previousDialog);

        fragmentTransaction.addToBackStack(null);

        SimpleDialog newFragmentDialog = new SimpleDialog();

        Bundle bundle = new Bundle();
        bundle.putString(Constants.DIALOG_TITLE, title);
        bundle.putString(Constants.DIALOG_MESSAGE, message);
        newFragmentDialog.setArguments(bundle);

        newFragmentDialog.show(fragmentTransaction, TAG);
    }

    private Listener listener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (Listener) context;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Bundle bundle = getArguments();
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext())
                .setIcon(R.drawable.ic_launcher_daynight)
                .setTitle(bundle.getString(Constants.DIALOG_TITLE))
                .setMessage(bundle.getString(Constants.DIALOG_MESSAGE))
                .setPositiveButton(R.string.ok, (dialog, which) ->
                        listener.onSimpleDialogOkPressed())
                .setNegativeButton(R.string.cancel, null);
        return builder.create();
    }

    public interface Listener {
        void onSimpleDialogOkPressed();
    }
}

