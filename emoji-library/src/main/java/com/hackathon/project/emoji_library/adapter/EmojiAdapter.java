package com.hackathon.project.emoji_library.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hackathon.project.emoji_library.R;
import com.hackathon.project.emoji_library.model.Emoji;

import java.util.List;

public final class EmojiAdapter extends RecyclerView.Adapter<EmojiAdapter.ViewHolder> {

    private OnClickListener listener;
    private Emoji[] data;
    private List<Emoji> data2;
    private boolean isArray;

    public EmojiAdapter(@NonNull OnClickListener listener, @NonNull Emoji[] data) {
        this.listener = listener;
        this.data = data;
        isArray = true;
    }

    public EmojiAdapter(@NonNull OnClickListener listener, @NonNull List<Emoji> data) {
        this.listener = listener;
        this.data2 = data;
        isArray = false;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.rsc_emoji_item, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (isArray) {
            holder.icon.setText(data[position].getEmoji());
        } else {
            holder.icon.setText(data2.get(position).getEmoji());
        }

        holder.icon.setOnClickListener(v -> {
            if (isArray)
                listener.onItemClicked(data[position]);
            else
                listener.onItemClicked(data2.get(position));
        });
    }

    @Override
    public int getItemCount() {
        if (isArray)
            return data.length;
        return data2.size();
    }

    static final class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView icon;

        ViewHolder(View itemView) {
            super(itemView);
            icon = itemView.findViewById(R.id.emoji_icon);
        }
    }

    public interface OnClickListener {
        void onItemClicked(@NonNull Emoji emoji);
    }
}
