package com.hackathon.project.emoji_library.controller;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hackathon.project.emoji_library.R;
import com.hackathon.project.emoji_library.adapter.EmojiAdapter;
import com.hackathon.project.emoji_library.model.Emoji;
import com.hackathon.project.emoji_library.model.OnEmojiClickListener;

public class FragmentEmoji extends Fragment implements EmojiAdapter.OnClickListener {

    private OnEmojiClickListener mOnEmojiconClickedListener;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mRootView = inflater.inflate(R.layout.frag_emoji, container, false);
        return mRootView;
    }

    public void addEmojiconClickListener(OnEmojiClickListener listener) {
        this.mOnEmojiconClickedListener = listener;
    }

    @Override
    public void onItemClicked(@NonNull Emoji emoji) {
        if (this.mOnEmojiconClickedListener != null) {
            this.mOnEmojiconClickedListener.onEmojiClicked(emoji);
        }
    }
}
