package com.hackathon.project.emoji_library.model.layout;


import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.hackathon.project.emoji_library.R;
import com.hackathon.project.emoji_library.controller.EmojiKeyboard;

public final class EmojiKeyboardLayout extends LinearLayout {
    public EmojiKeyboardLayout(Context context) {
        super(context);
        this.init(context);
    }

    public EmojiKeyboardLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.init(context);
    }

    public EmojiKeyboardLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.init(context);
    }

    public EmojiKeyboardLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.init(context);
    }

    private void init(Context context) {
        LayoutInflater.from(context).inflate(R.layout.rsc_emoji_keyboard, this, true);
        this.findViewById(R.id.emoji_keyboard).setVisibility(RelativeLayout.VISIBLE);
    }

    public void prepareKeyboard(AppCompatActivity activity, EditText input) {
        new EmojiKeyboard(activity, input);
    }
}
