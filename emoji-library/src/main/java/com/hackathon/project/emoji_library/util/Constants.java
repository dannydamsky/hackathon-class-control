package com.hackathon.project.emoji_library.util;

public final class Constants {
    public static final String EMOJI_KEY = "emojic";

    private Constants() {
    }
}
