package com.hackathon.project.emoji_library.model;

public interface OnEmojiClickListener {
    void onEmojiClicked(Emoji emojicon);
}
