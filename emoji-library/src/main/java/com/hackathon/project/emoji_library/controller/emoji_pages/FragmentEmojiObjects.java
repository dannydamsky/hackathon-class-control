package com.hackathon.project.emoji_library.controller.emoji_pages;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hackathon.project.emoji_library.R;
import com.hackathon.project.emoji_library.adapter.EmojiAdapter;
import com.hackathon.project.emoji_library.controller.FragmentEmoji;
import com.hackathon.project.emoji_library.model.Emoji;
import com.hackathon.project.emoji_library.model.Objects;
import com.hackathon.project.emoji_library.util.Constants;

public final class FragmentEmojiObjects extends FragmentEmoji {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.frag_emoji, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        RecyclerView recyclerView = view.findViewById(R.id.Emoji_GridView);
        recyclerView.setHasFixedSize(true);
        Bundle bundle = getArguments();
        Emoji[] mData;
        if (bundle == null) {
            mData = Objects.DATA;
        } else {
            Parcelable[] parcels = bundle.getParcelableArray(Constants.EMOJI_KEY);
            mData = new Emoji[parcels.length];
            for (int i = 0; i < parcels.length; i++) {
                mData[i] = (Emoji) parcels[i];
            }
        }
        recyclerView.setAdapter(new EmojiAdapter(this, mData));
    }
}
