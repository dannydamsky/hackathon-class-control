package com.hackathon.project.emoji_library.controller;

import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.widget.EditText;
import android.widget.ImageView;

import com.hackathon.project.emoji_library.R;
import com.hackathon.project.emoji_library.adapter.EmojiTabAdapter;
import com.hackathon.project.emoji_library.model.Emoji;
import com.hackathon.project.emoji_library.model.OnEmojiClickListener;


public final class EmojiKeyboard implements OnEmojiClickListener {
    private AppCompatActivity mActivity;

    private ConstraintLayout mEmojiKeyboardLayout;
    private EditText mInput;
    private ImageView mBackspace;

    public EmojiKeyboard(AppCompatActivity activity, EditText input) {
        this.mInput = input;
        this.mActivity = activity;
        this.mEmojiKeyboardLayout = mActivity.findViewById(R.id.emoji_keyboard);
        this.initEmojiKeyboardViewPager();
        this.setBackspaceBehaviour();
    }

    private void initEmojiKeyboardViewPager() {
        final EmojiTabAdapter adapter = new EmojiTabAdapter(mActivity.getSupportFragmentManager());
        adapter.setOnEmojiClickListener(this);
        final ViewPager viewPager = mActivity.findViewById(R.id.emoji_viewpager);
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(5);
        TabLayout viewPagerTab = mActivity.findViewById(R.id.emoji_tabs);
        viewPagerTab.setupWithViewPager(viewPager);
        viewPagerTab.getTabAt(0).setIcon(R.drawable.ic_emoji_people);
        viewPagerTab.getTabAt(1).setIcon(R.drawable.ic_emoji_nature);
        viewPagerTab.getTabAt(2).setIcon(R.drawable.ic_emoji_objects);
        viewPagerTab.getTabAt(3).setIcon(R.drawable.ic_emoji_places);
        viewPagerTab.getTabAt(4).setIcon(R.drawable.ic_emoji_symbols);
    }

    private void setBackspaceBehaviour() {
        this.mBackspace = mActivity.findViewById(R.id.backspace);
        this.mBackspace.setOnClickListener(v ->
                EmojiKeyboard.this.mInput.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_DEL)));
        this.mBackspace.setOnLongClickListener(v -> {
            mInput.setText("");
            return false;
        });
    }

    public ConstraintLayout getEmojiKeyboardLayout() {
        return mEmojiKeyboardLayout;
    }

    @Override
    public void onEmojiClicked(Emoji emojicon) {
        int start = this.mInput.getSelectionStart();
        int end = this.mInput.getSelectionEnd();

        if (start < 0) {
            this.mInput.append(emojicon.getEmoji());
        } else {
            this.mInput.getText().replace(Math.min(start, end), Math.max(start, end), emojicon.getEmoji(), 0, emojicon.getEmoji().length());
        }
    }
}
