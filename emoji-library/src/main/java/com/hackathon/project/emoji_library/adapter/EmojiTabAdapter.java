package com.hackathon.project.emoji_library.adapter;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.hackathon.project.emoji_library.controller.emoji_pages.FragmentEmojiNature;
import com.hackathon.project.emoji_library.controller.emoji_pages.FragmentEmojiObjects;
import com.hackathon.project.emoji_library.controller.emoji_pages.FragmentEmojiPeople;
import com.hackathon.project.emoji_library.controller.emoji_pages.FragmentEmojiPlaces;
import com.hackathon.project.emoji_library.controller.emoji_pages.FragmentEmojiSymbols;
import com.hackathon.project.emoji_library.model.OnEmojiClickListener;

public final class EmojiTabAdapter extends FragmentPagerAdapter {

    private final FragmentEmojiPeople fragmentEmojiPeople = new FragmentEmojiPeople();
    private final FragmentEmojiNature fragmentEmojiNature = new FragmentEmojiNature();
    private final FragmentEmojiObjects fragmentEmojiObjects = new FragmentEmojiObjects();
    private final FragmentEmojiPlaces fragmentEmojiPlaces = new FragmentEmojiPlaces();
    private final FragmentEmojiSymbols fragmentEmojiSymbols = new FragmentEmojiSymbols();


    public EmojiTabAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "";
    }

    @Override
    public int getCount() {
        return 5;
    }

    @Override
    public android.support.v4.app.Fragment getItem(int position) {
        switch (position) {
            case 0:
                return fragmentEmojiPeople;
            case 1:
                return fragmentEmojiNature;
            case 2:
                return fragmentEmojiObjects;
            case 3:
                return fragmentEmojiPlaces;
            case 4:
                return fragmentEmojiSymbols;
            default:
                return fragmentEmojiPeople;
        }
    }

    public void setOnEmojiClickListener(OnEmojiClickListener listener) {
        fragmentEmojiPeople.addEmojiconClickListener(listener);
        fragmentEmojiNature.addEmojiconClickListener(listener);
        fragmentEmojiObjects.addEmojiconClickListener(listener);
        fragmentEmojiPlaces.addEmojiconClickListener(listener);
        fragmentEmojiSymbols.addEmojiconClickListener(listener);
    }
}
